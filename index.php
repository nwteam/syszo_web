<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

	$url = API_PATH.API_KNOW_INFO_LIST;

	//緊急トラブル発生中！
	$post_data['p_size'] = 5;//表示件数
	$post_data['type'] = 4;//トラブル発生中

	$o = "";
	foreach ( $post_data as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data = substr($o,0,-1);
	$res = request_post($url, $post_data);
	$urgent_json = json_decode($res,TRUE);
	//echo var_dump($urgent_json);

	$result = $urgent_json['result'];
	$msg = $urgent_json['msg'];
	if($result!="0"){
		$count_urgent = count($urgent_json["data"]);
	}

	//新着の投稿
	$post_data_new['p_size'] = 12;//表示件数
	$post_data_new['type'] = 1;//新着順

	$o = "";
	foreach ( $post_data_new as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data_new = substr($o,0,-1);
	$res = request_post($url, $post_data_new);
	$new_json = json_decode($res,TRUE);
	//echo var_dump($new_json);

	$result = $new_json['result'];
	$msg = $new_json['msg'];
	if($result!="0"){
		$count_new = count($new_json["data"]);
	}

	//最新のコメント順
	$post_data_replay['p_size'] = 12;//表示件数
	$post_data_replay['type'] = 2;//最新のコメント順

	$o = "";
	foreach ( $post_data_replay as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data_replay = substr($o,0,-1);
	$res = request_post($url, $post_data_replay);
	$replay_json = json_decode($res,TRUE);
	//echo var_dump($replay_json);

	$result = $replay_json['result'];
	$msg = $replay_json['msg'];
	if($result!="0"){
		$count_replay = count($replay_json["data"]);
	}
?>
<?php include "head.php"; ?>
<?php
	if($_SESSION['user_id']==""&&$_COOKIE['user_id']==""){
?>
<!--151217追加-->
<script src="js/jquery.cookie.js"></script>
<script src="js/jquery.layerBoard.js"></script>
<script>
$(function(){
	$('#layer_board_area').layerBoard({alpha:0.5});
})
</script>
<!--151217追加ここまで-->
<?php
}
?>
</head>
<body>
<?php
	if($_SESSION['user_id']==""&&$_COOKIE['user_id']==""){
?>
<!--151217追加-->
<div id="layer_board_area">
  <div class="layer_board_bg"></div>
  <div class="layer_board">
  <p class="close btn_close"><img src="images/close.png" alt="close"/></p>
  <section id="popup">
	  <h2>ログイン・会員登録</h2>
	  <div id="popupInner">
		  <div id="popUpLeft">
			  <!--ログイン-->
			  <h3>ログイン</h3>
			  <form id="form3">
				  <dt id="info_login"></dt>
				  <dl>
					  <?php if($result=="0"){echo "<dt  style='color:red;'>$msg</dt>";}?>
					  <dt>メールアドレス</dt>
					  <dd>
						  <input type="text" name="user_email" id="user_email_login" maxlength="100" size="40" value="<?php echo $user_email;?>" />
					  </dd>
					  <dt>パスワード</dt>
					  <dd>
						  <input type="password" name="user_pwd" id="user_pwd_login" size="40" value="<?php echo $user_pwd;?>" />
						  <p class="note">※6〜16文字</p>
					  </dd>
				  </dl>
				  <p id="check">
					  <input name="contact" type="checkbox" value="1" id="check-1" />
					  <label for="check-1">次回から自動ログイン</label>
				  </p>
				  <div id="submit"><input type="button" onclick="validation_login();" value="ログイン" /></div>
			  </form>
			  <p id="forget">パスワードを忘れた方は<a href="password.php">パスワードの再設定</a>を行ってください。</p>
		  </div>
		  <div id="popUpRignt">
			  <!--会員登録-->
			  <h3>会員登録</h3>
			  <form id="form2">
				  <dt id="info"></dt>
				  <dl>
					  <dt>ニックネーム</dt>
					  <dd><input type="text" id="user_nick" maxlength="100" size="40" value="<?php echo $user_nick;?>" /></dd>
					  <dt>メールアドレス</dt>
					  <dd><input type="text" id="user_email" maxlength="100" size="40" value="<?php echo $user_email;?>" /></dd>
					  <dt>パスワード</dt>
					  <dd><input type="password" id="user_pwd" size="40" value="" />
						  <p class="note">※6〜16文字</p>
					  </dd>
				  </dl>
				  <div id="submit"><input type="button" onclick="validation();" value="登録する" /></div>
			  </form>
		  </div>

	  </div>
  </section>
  <section id="mypage" style="display: none;">
	  <h2>会員登録</h2>
	  <div id="myPageInner">
		  <!--Thanks-->
		  <h3>ご登録いただきありがとうございました。<br>
			  こちらのアカウントでアプリもご利用いただけます。<br>
			  <img src="images/appicon.png" alt="syszoアプリ"/>
		  </h3>
		  <!--/Thanks-->
	  </div>
  </section>
  </div>
</div>
<!--151217追加ここまで-->
<?php
}
?>
<?php include "header.php"; ?>
<div id="wrapper">
  <div id="contents">
		<?php include "nav.php"; ?>
    <section id="emergencyArea">
      <h2>緊急の投稿</h2>
      <p class="btnPosts"><a href="emergency.php">緊急一覧</a></p>
      <ul>
				<?php for ($i = 0; $i < $count_urgent; $i++){ ?>
					<li <?php if($urgent_json["data"][$i]['urgent']=="2") {echo "class='emergency'";}?>>
						<a href="detail.php?id=<?php echo $urgent_json["data"][$i]['know_id'];?>">
						<span class="postTitle"><?php echo $urgent_json["data"][$i]["title"];?></span>
						<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($urgent_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#emergencyArea-->
    <section id="newPosts">
      <h2>新着の投稿</h2>
      <p class="btnPosts"><a href="new.php">投稿一覧</a></p>
      <ul>
				<?php for ($i = 0; $i < $count_new; $i++){ ?>
					<li <?php if($new_json["data"][$i]['urgent']=="2") {echo "class=''";}?>>
						<a href="detail.php?id=<?php echo $new_json["data"][$i]['know_id'];?>">
						<span class="postTitle"><?php echo $new_json["data"][$i]["title"];?></span>
						<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($new_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#newPosts-->
    <section id="answer">
      <h2>最新のコメント順</h2>
      <p class="btnPosts"><a href="answer.php">回答一覧</a></p>
      <ul>
				<?php for ($i = 0; $i < $count_replay; $i++){ ?>
					<li <?php if($replay_json["data"][$i]['urgent']=="2") {echo "class=''";}?>>
						<a href="detail.php?id=<?php echo $replay_json["data"][$i]['know_id'];?>">
						<span class="postTitle"><?php echo $replay_json["data"][$i]["title"];?></span>
						<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($replay_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#answer--> 
    
  </div>
  <!--/#contents-->
	<?php include "side.php"; ?> 
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
<script type="text/javascript">
function validation(){
	var user_nick = document.getElementById("user_nick").value;
	var user_email = document.getElementById("user_email").value;
	var user_pwd = document.getElementById("user_pwd").value;
	var postStr = "user_nick="+user_nick+"&user_email="+user_email+"&user_pwd="+user_pwd;
	ajax("pop_reg.php?action=reg",postStr,function(result){
		if(result=="success"){

			//$("#popUpLeft").css({ display: "none" });
			//$("#popUpRignt").css({ display: "none" });
			$("#popup").css({ display: "none" });
			$("#mypage").css({ display: "block" });
			//document.getElementById("tks").innerHTML="ご登録いただきありがとうございました。<br>こちらのアカウントでアプリもご利用いただけます。<br><img src='images/appicon.png' alt='syszoアプリ'/>";
			//google
			ga('send', 'event', 'button', 'click', 'ポップアップ新規登録');
			//$("#layer_board_area").fadeOut(5000);
			//window.location.href="/"; 
			$('.btn_close').click(function() {
				window.location.href="/"; 
			});
		}else{
			$("#info").css({ color: "red", display: "inline" });
			document.getElementById("info").innerHTML=result;
			$("#info").fadeOut(5000);
		}
	});
}
function validation_login(){
	var user_email = document.getElementById("user_email_login").value;
	var user_pwd = document.getElementById("user_pwd_login").value;
	var contact = document.getElementById("check-1").value;
	var postStr = "user_email="+user_email+"&user_pwd="+user_pwd+"&contact="+contact;
	ajax("pop_login.php?action=login",postStr,function(result){
		if(result=="success"){
			//google
			ga('send', 'event', 'button', 'click', 'ポップアップログイン');
			//$("#layer_board_area").fadeOut(5000);
			window.location.href="/";
		}else{
			$("#info_login").css({ color: "red", display: "inline" });
			document.getElementById("info_login").innerHTML=result;
			$("#info_login").fadeOut(5000);
		}
	});
}
function ajax(url,postStr,onsuccess){
	var xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	xmlhttp.open("POST", url, true);
	xmlhttp.onreadystatechange = function ()
	{
			if (xmlhttp.readyState == 4)
			{
					if (xmlhttp.status == 200)
					{
							onsuccess(xmlhttp.responseText);
					}
					else
					{
							alert("ERROR!");
					}
			}
	}
	xmlhttp.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	xmlhttp.send(postStr);
}
</script>
</body>
</html>