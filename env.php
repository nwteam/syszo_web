<?php
session_start();
//release
define("ADMIN_MAIL", "master@syszo.com");
define("HOME_PAGE","http://syszo.com/");
define("API_PATH","http://api.syszo.com/");
define("DOCUMENT_ROOT", "/home/joushisu/public_html/api/");
define("DIR_IMG_PATH","upload/");

define("API_KNOW_SEARCH_INDEX","index.php/know/search_index");

define("API_LOGIN","index.php/login/login");
define("API_LOGIN_REG","index.php/login/reg");
define("API_LOGIN_GET_PWD","index.php/login/get_pwd");
define("API_KNOW_INFO_LIST","index.php/know/info_list");
define("API_KNOW_INFO","index.php/know/info");
define("API_KNOW_COMMENTS_LIST","index.php/know/comments_list");
define("API_KNOW_COMMENTS","index.php/know/comments");
define("API_KNOW_GOOD","index.php/know/good");
define("API_KNOW_GET_GOOD_COUNT","index.php/know/get_good_count");
define("API_KNOW_SEARCH","index.php/know/search");
define("API_KNOW_ADD","index.php/know/add");
define("API_KNOW_SEND_NOTI_ALL","index.php/know/send_noti_all");
define("API_KNOW_SEND_NOTI_URGENT","index.php/know/send_noti_urgent");

define("API_MYSELF_MYDATA","index.php/myself/mydata");
define("API_MYSELF_MYDATA_EDIT","index.php/myself/mydata_edit");
define("API_MYSELF_PWD_EDIT","index.php/myself/pwd_edit");

define("API_MYSELF_MYDATA_GET_ADDRESS","index.php/myself/get_address");//获取地址列表
define("API_MYSELF_MYDATA_GET_JOB","index.php/myself/get_job");//获取职务列表
define("API_MYSELF_MYDATA_GET_USE","index.php/myself/get_use");//获取情シス歴列表
define("API_MYSELF_MYDATA_GET_INDUSTRY","index.php/myself/get_industry");// 获取职业种类列表
define("API_MYSELF_MYDATA_GET_SIZE_COMPANY","index.php/myself/get_size_company");//获取公司规模列表
define("API_MYSELF_MYDATA_GET_REG_MONEY","index.php/myself/get_reg_money");// 获取注册资金列表
define("API_MYSELF_MYDATA_GET_SOFTWARE","index.php/myself/get_software_web");//获取软件列表
define("API_MYSELF_MYDATA_GET_HARDWARE","index.php/myself/get_hardware_web");//获取硬件列表
define("API_MYSELF_MYDATA_GET_QUALIFIED","index.php/myself/get_qualified_web");//获取拥有资格列表
define("API_MYSELF_MYDATA_GET_ALLYEAR","index.php/myself/get_allyear");//获取全年预算列表

define("API_UPLOAD_INDEX","index.php/upload/index");


?>