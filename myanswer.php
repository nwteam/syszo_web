<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

$login_user_id=$_SESSION['user_id'];
if($login_user_id==""){$login_user_id=$_COOKIE['user_id'];}
$login_user_name=$_SESSION['user_nick'];
if($login_user_name==""){$login_user_name=$_COOKIE['user_nick'];}

$url = API_PATH.API_KNOW_INFO_LIST;

//回答を見る
$post_data_replay['p_size'] = 1000000;//表示件数
$post_data_replay['type'] = 3;//回答を見る
$post_data_replay['user_id'] = $login_user_id;//user_id

$o = "";
foreach ( $post_data_replay as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_replay = substr($o,0,-1);
$res = request_post($url, $post_data_replay);
$replay_json = json_decode($res,TRUE);
//投稿したコメント時間（API修正済）で降順
 //   usort($replay_json["data"], function ($a, $b) use ("time") {
 //       if ($a["time"] == $b["time"] return 0;
 //       return ($a["time"] > $b["time"]) ? -1 : 1;
 //   });
//echo var_dump($replay_json);

$result = $replay_json['result'];
$msg = $replay_json['msg'];
if($result!="0"){
	$count_replay = count($replay_json["data"]);
}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <div id="contents">
		<?php include "nav.php"; ?>
    <section id="answer">
      <h2>じぶんの投稿</h2>
      <ul>
				<?php for ($i = 0; $i < $count_replay; $i++){ ?>
					<li <?php if($replay_json["data"][$i]['urgent']=="2") {echo "class='emergency'";}?>>
						<a href="detail.php?id=<?php echo $replay_json["data"][$i]['know_id'];?>">
						<span class="postTitle"><?php echo $replay_json["data"][$i]["title"];?></span>
						<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($replay_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#newPosts--> 
    
  </div>
  <!--/#contents-->
	<?php include "side.php"; ?>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>