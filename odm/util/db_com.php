<?php
function quote_smart($value,$strFlg)
{
    // 数値以外をクオートする
    if (!is_numeric($value) || $strFlg) {
        $value = "'" . mysql_real_escape_string($value) . "'";
    }
    return $value;
}
function db_conn(){

    $db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
    if(!$db){
        die("connot connect:" . mysql_error());
    }

    $dns = mysql_select_db(DB_NAME,$db);

    if(!$dns){
        die("connot use db:" . mysql_error());
    }


    return $db;
}
function db_disConn($result,$db){

    //mysql_free_result($result);
    mysql_close($db);

}
//倒産情報存在チェック
function searchCloseCompany($company_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from app_closure where company_name=%s",quote_smart($company_name,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//倒産情報登録処理
function addCloseCompany($seili_YYYYMM,$company_name){

    $systime=date('Y-m-d H:i:s',time());
    $rowCnt = 0;

    $logstr = "▼倒産情報登録処理開始 ".$systime."：整理年月=".$seili_YYYYMM." 倒産企業名＝".$company_name."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sql = sprintf("INSERT INTO app_closure(company_name,close_time,insert_time) VALUES (%s,%d,%d)",
        quote_smart($company_name,true),
        $seili_YYYYMM,
        strtotime($systime)
    );
    $logstr = "SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        $logstr = "★倒産情報登録処理異常！！ ".$systime."：整理年月=".$seili_YYYYMM." 倒産企業名＝".$company_name."\r\n"."SQL文：".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    mysql_close($link);
    $logstr = "▲倒産情報登録処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}
//企業情報存在チェック
function searchCompanyInfo($company_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from app_company where company_name=%s",quote_smart($company_name,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//企業情報登録処理
function addCompanyInfo($area_id,$company_name){

    $systime=date('Y-m-d H:i:s',time());
    $rowCnt = 0;

    $logstr = "▼企業情報登録処理開始 ".$systime."：地域ID=".$area_id." 企業名＝".$company_name."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sql = sprintf("INSERT INTO app_company(company_name,type,insert_time,city_id) VALUES (%s,%d,%d,%d)",
        quote_smart($company_name,true),
        2,
        strtotime($systime),
        $area_id
    );
    $logstr = "SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        $logstr = "★企業情報登録処理異常！！ ".$systime."：地域ID=".$area_id." 企業名＝".$company_name."\r\n"."SQL文：".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    mysql_close($link);
    $logstr = "▲企業情報登録処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}
//現場情報存在チェック
function searchGenInfo($gen_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from app_workp where gen_name=%s",quote_smart($gen_name,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//現場情報登録処理
function addGenInfo($area_id,$company_id,$gen_name){

    $systime=date('Y-m-d H:i:s',time());
    $rowCnt = 0;

    $logstr = "▼現場情報登録処理開始 ".$systime."：地域ID=".$area_id." 企業ID＝".$company_id." 現場名＝".$gen_name."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sql = sprintf("INSERT INTO app_workp(title,user_id,area_id,company_id,insert_time,status) VALUES (%s,%d,%d,%d,%d,%d)",
        quote_smart($gen_name,true),
        999999999,
        $area_id,
        $company_id,
        strtotime($systime),1
    );
    $logstr = "SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        $logstr = "★現場情報登録処理異常！！ ".$systime."：地域ID=".$area_id." 企業ID＝".$company_id." 現場名＝".$gen_name."\r\n"."SQL文：".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    mysql_close($link);
    $logstr = "▲現場情報登録処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}

//俺のシルーズ分類情報存在チェック
function searchseriesInfo($series_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from  app_my_series where category_name=%s",quote_smart($series_name,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//俺ののシリーズ分類情報登録処理
function addseriesInfo($series_name){

    $systime=date('Y-m-d H:i:s',time());
    $rowCnt = 0;

    $logstr = "▼俺のシリーズ分類情報登録処理開始 ".$systime."：俺のシリーズ分類名＝".$series_name."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sql = sprintf("INSERT INTO app_my_series(category_name,user_id,insert_time,status) VALUES (%s,%d,%d,%d)",
        quote_smart($series_name,true),
        999999999,
        strtotime($systime),1
    );
    $logstr = "SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        $logstr = "★俺のシリーズ分類情報登録処理異常！！ ".$systime."：俺のシリーズ分類名＝".$series_name."\r\n"."SQL文：".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    mysql_close($link);
    $logstr = "▲俺のシリーズ分類情報登録処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}
//カテゴリ情報存在チェック
function searchCategoryInfo($category_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from  app_news_category where category_name=%s",quote_smart($category_name,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//カテゴリ情報登録処理
function addCategoryInfo($category_name){

    $systime=date('Y-m-d H:i:s',time());
    $rowCnt = 0;

    $logstr = "▼カテゴリ情報登録処理開始 ".$systime."：俺のシリーズ分類名＝".$category_name."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sql = sprintf("INSERT INTO app_news_category(category_name,insert_time) VALUES (%s,%d)",
        quote_smart($category_name,true),
        strtotime($systime)
    );
    $logstr = "SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        $logstr = "★カテゴリ情報登録処理異常！！ ".$systime."：カテゴリ名＝".$category_name."\r\n"."SQL文：".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    mysql_close($link);
    $logstr = "▲カテゴリ情報登録処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}
//ユーザー情報存在チェック
function searchUserInfo($user_nick,$user_email){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from  app_member where (user_nick=%s or user_email=%s)",quote_smart($user_nick,true),quote_smart($user_email,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//ユーザー情報登録処理
function addUserInfo($user_nick,$user_email,$user_pwd){

    $systime=date('Y-m-d H:i:s',time());
    $rowCnt = 0;
		$user_pwd=md5($user_pwd);

    $logstr = "▼ユーザー情報登録処理開始 ".$systime."：ユーザー名＝".$user_nick."email＝".$user_email."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sql = sprintf("INSERT INTO app_member(user_nick,user_email,user_pwd,insert_time) VALUES (%s,%s,%s,%d)",
        quote_smart($user_nick,true),
        quote_smart($user_email,true),
        quote_smart($user_pwd,true),
        strtotime($systime)
    );
    $logstr = "SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        $logstr = "★ユーザー情報登録処理異常！！ ".$systime."：ユーザー名＝".$user_nick."email＝".$user_email."\r\n"."SQL文：".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    mysql_close($link);
    $logstr = "▲ユーザー情報登録処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}
//スポーツカテゴリ情報存在チェック
function searchSportsCategoryInfo($category_name){
    $link = db_conn();
    mysql_set_charset('utf8');

    $rowCnt = 0;

    $sql = sprintf("SELECT * from  app_news_two_category where category_name=%s",quote_smart($category_name,true));

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        return $rowCnt;
    }

    $rowCnt=mysql_num_rows($result);

    if($rowCnt==0){
        db_disConn($result, $link);
        return $rowCnt;
    }else{
        $rowCnt=1;
    }

    mysql_free_result($result);
    mysql_close($link);

    return $rowCnt;
}
//スポーツカテゴリ情報登録処理
function addSportsCategoryInfo($category_name){

    $systime=date('Y-m-d H:i:s',time());
    $rowCnt = 0;

    $logstr = "▼スポーツカテゴリ情報登録処理開始 ".$systime."：俺のシリーズ分類名＝".$category_name."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sql = sprintf("INSERT INTO app_news_two_category(category_name,insert_time) VALUES (%s,%d)",
        quote_smart($category_name,true),
        strtotime($systime)
    );
    $logstr = "SQL文： ".$sql."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result = mysql_query($sql,$link);
    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
        $logstr = "★スポーツカテゴリ情報登録処理異常！！ ".$systime."：カテゴリ名＝".$category_name."\r\n"."SQL文：".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    mysql_close($link);
    $logstr = "▲スポーツカテゴリ情報登録処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}
//プッシュ通知送信処理
function sendPushInfo($add_msg,$add_area_id){

    $systime=date('Y-m-d H:i:s',time());

    $rowCnt = 0;

    $logstr = "▼プッシュ通知送信処理開始 ".$systime."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $link = db_conn();
    mysql_set_charset('utf8');

    $sqlall = "select  device_id,user_id,device,user_nick,user_email,insert_time from app_member WHERE 1 and device_id <>'' ";

    if($add_area_id!='') {
        $sqlall .= " and address = $add_area_id";
    }


    $logstr = "プッシュ通知送信対象検索SQL文： ".$sqlall."\r\n";
    error_log($logstr,3,'../log/gen.log');

    $result_am = mysql_query($sqlall,$link) or die(mysql_error());
    $rowCnt_push=mysql_num_rows($result_am);

    $logstr = "プッシュ通知送信対象件数： ".$rowCnt_push."\r\n";
    error_log($logstr,3,'../log/gen.log');

    if(!$result_am){
        $rowCnt = -1;
        db_disConn($result_am, $link);
        $logstr = "★プッシュ通知送信対象検索処理異常！！ ".$systime."SQL文：".$sqlall."\r\n";
        error_log($logstr,3,'../log/gen.log');
        return $rowCnt;
    }

    while($rs=mysql_fetch_object($result_am))
    {
        $u_id=$rs->user_id;
//		$device_id=$rs->device_id;
		
        $post_data = array();
        $post_data['user_id'] = '999999999';
        $post_data['group_id'] = '0';
        $post_data['rec_id'] = $u_id;
        $post_data['info_content'] = $add_msg;
//        $post_data['device_id'] = $device_id;
		$logstr = "★".$u_id."\r\n";
        error_log($logstr,3,'../log/gen.log');
        $url=IMG_URL_PATH.'/index.php/message/send_admin_msg';
        $o="";
        foreach ($post_data as $k=>$v)
        {
            $o.= "$k=".urlencode($v)."&";
        }
        $post_data=substr($o,0,-1);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        $result_post = curl_exec($ch);
        if(!$result_post){
            $rowCnt = -1;
            db_disConn($result_post, $link);
            $logstr = "★プッシュ通知送信処理異常！！ ".$systime."\r\n";
            error_log($logstr,3,'../log/gen.log');
            return $rowCnt;
        }
    }
    if($rowCnt_push>0){
        $sql = sprintf("INSERT INTO app_push_history(message,area_id,insert_time)
                        VALUES (%s,%d,%d)",
            quote_smart($add_msg,true),
            $add_area_id,
            strtotime($systime)
        );
        $logstr = "プッシュ通知送信履歴登録SQL文： ".$sql."\r\n";
        error_log($logstr,3,'../log/gen.log');

        $result_aph = mysql_query($sql,$link);
        if(!$result_aph){
            $rowCnt = -1;
            db_disConn($result_aph, $link);
            $logstr = "★プッシュ通知送信履歴登録処理異常！！ ".$systime."SQL文：".$sql."\r\n";
            error_log($logstr,3,'../log/gen.log');
            return $rowCnt;
        }
    }

    mysql_close($link);
    $logstr = "▲プッシュ通知送信処理正常終了！！\r\n";
    error_log($logstr,3,'../log/gen.log');
    return $rowCnt;
}
?>