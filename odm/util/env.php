<?php
session_start();

 	define("DB_HOST", "localhost");
 	define("DB_NAME", "syszo");
 	define("DB_USER", "www");
 	define("DB_PASS", "kanri2016@");

//release
 define("ADMIN_MAIL", "master@syszo.com");
 define("ADMIN_MAIL_TITLE", "");

define("URL_PATH","https://syszo.com/odm/mg/");
define("IMG_URL_PATH","https://syszo.com");

define("MAX_FILE_SIZE", 10485760);
define("NEWS_UPLOAD_FOLDER", "/upload/news/");
define("NEWS_UPLOAD_FOLDER_ROOT", "../../upload/news/");

define("SPORTS_NEWS_UPLOAD_FOLDER", "/upload/sports_news/");
define("SPORTS_NEWS_UPLOAD_FOLDER_ROOT", "../../upload/sports_news/");

define("C_NAME_01","北海道");
define("C_NAME_02","青森県");
define("C_NAME_03","秋田県");
define("C_NAME_04","岩手県");
define("C_NAME_05","山形県");
define("C_NAME_06","宮城県");
define("C_NAME_07","福島県");
define("C_NAME_08","新潟県");
define("C_NAME_09","富山県");
define("C_NAME_10","石川県");
define("C_NAME_11","群馬県");
define("C_NAME_12","栃木県");
define("C_NAME_13","長野県");
define("C_NAME_14","岐阜県");
define("C_NAME_15","埼玉県");
define("C_NAME_16","茨城県");
define("C_NAME_17","東京都");
define("C_NAME_18","千葉県");
define("C_NAME_19","神奈川県");
define("C_NAME_20","静岡県");
define("C_NAME_21","山梨県");
define("C_NAME_22","愛知県");
define("C_NAME_23","福井県");
define("C_NAME_24","滋賀県");
define("C_NAME_25","三重県");
define("C_NAME_26","京都府");
define("C_NAME_27","奈良県");
define("C_NAME_28","兵庫県");
define("C_NAME_29","大阪府");
define("C_NAME_30","和歌山県");
define("C_NAME_31","島根県");
define("C_NAME_32","鳥取県");
define("C_NAME_33","岡山県");
define("C_NAME_34","広島県");
define("C_NAME_35","山口県");
define("C_NAME_36","香川県");
define("C_NAME_37","愛媛県");
define("C_NAME_38","徳島県");
define("C_NAME_39","高知県");
define("C_NAME_40","大分県");
define("C_NAME_41","福岡県");
define("C_NAME_42","佐賀県");
define("C_NAME_43","熊本県");
define("C_NAME_44","長崎県");
define("C_NAME_45","宮崎県");
define("C_NAME_46","鹿児島県");
define("C_NAME_47","沖縄県");

define("C_ERR_99","システムエラーが発生しました。サイト管理者にお問い合わせください。");
define("C_ERR_01","ファイルアップロード異常が発生しました。サイト管理者にお問い合わせください。");


$arr_err_msg = array("99"=>C_ERR_99,"01"=>C_ERR_01,"02"=>C_ERR_02,"03"=>C_ERR_03,"04"=>C_ERR_04,
		"05"=>C_ERR_05,"06"=>C_ERR_06,"07"=>C_ERR_07,"08"=>C_ERR_08,"09"=>C_ERR_09,
		"10"=>C_ERR_10,"11"=>C_ERR_11,"12"=>C_ERR_12,"13"=>C_ERR_13,"14"=>C_ERR_14,"15"=>C_ERR_15,
		"16"=>C_ERR_16,"17"=>C_ERR_17,"18"=>C_ERR_18,"19"=>C_ERR_19,"94"=>C_ERR_94,"95"=>C_ERR_95,
                "96"=>C_ERR_96,"97"=>C_ERR_97,"98"=>C_ERR_98);

$arr_city_name = array(
						"1"=>C_NAME_01,"2"=>C_NAME_02,"3"=>C_NAME_03,"4"=>C_NAME_04,"5"=>C_NAME_05,
						"6"=>C_NAME_06,"7"=>C_NAME_07,"8"=>C_NAME_08,"9"=>C_NAME_09,"10"=>C_NAME_10,
						"11"=>C_NAME_11,"12"=>C_NAME_12,"13"=>C_NAME_13,"14"=>C_NAME_14,"15"=>C_NAME_15,
						"16"=>C_NAME_16,"17"=>C_NAME_17,"18"=>C_NAME_18,"19"=>C_NAME_19,"20"=>C_NAME_20,
						"21"=>C_NAME_21,"22"=>C_NAME_22,"23"=>C_NAME_23,"24"=>C_NAME_24,"25"=>C_NAME_25,
						"26"=>C_NAME_26,"27"=>C_NAME_27,"28"=>C_NAME_28,"29"=>C_NAME_29,"30"=>C_NAME_30,
						"31"=>C_NAME_31,"32"=>C_NAME_32,"33"=>C_NAME_33,"34"=>C_NAME_34,"35"=>C_NAME_35,
						"36"=>C_NAME_36,"37"=>C_NAME_37,"38"=>C_NAME_38,"39"=>C_NAME_39,"40"=>C_NAME_40,
						"41"=>C_NAME_41,"42"=>C_NAME_42,"43"=>C_NAME_43,"44"=>C_NAME_44,"45"=>C_NAME_45,
						"46"=>C_NAME_46,"47"=>C_NAME_47
                );

?>