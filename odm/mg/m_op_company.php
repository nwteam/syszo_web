<?php
	//運営管理⇒現場の掃き溜めに会社・現場（都道府県）追加管理画面
	//include
	require '../util/include.php';

	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='運営管理メニュー';
	$f_page_url=URL_PATH.'m_op.php';
	$page_name='現場の掃き溜めに会社追加管理画面';

	$action = $_GET['action'];

	$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
	if(!$db){
		die("connot connect:" . mysql_error());
	}

	$dns = mysql_select_db(DB_NAME,$db);

	if(!$dns){
		die("connot use db:" . mysql_error());
	}

	mysql_set_charset('utf8');

    $sqlall = "select * from app_area WHERE 1";
    $result_area = mysql_query($sqlall,$db);

	mysql_close($db);

	//Delete
	if ($action=='delete'){
		$c_id = $_GET['c_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from app_company WHERE company_id = %d",$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Update
	if ($action=='update'){
		$c_id = $_GET['c_id'];
		$up_company_name = $_GET['name'];
        $up_area_id = $_GET['area_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE app_company SET company_name ='%s',city_id=%d WHERE company_id = %d",$up_company_name,$up_area_id,$c_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if (($action=='search')||($action=='delete')||($action=='update')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=10;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$city_id=$_POST['city_id'];
		if($city_id==''){
            $city_id=$_GET['city_id'];
        }
		$company_name = $_POST['company_name'];
        if($company_name==''){
            $company_name=$_GET['c_name'];
        }

		//All
		$sqlall = "select ac.* from app_company ac WHERE 1";

		if($city_id!='') {
			$sqlall .= " and ac.city_id = $city_id";
		}
		if($company_name!='') {
			$sqlall .= " and ac.company_name like '%$company_name%'";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by company_id desc  limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1&city_id='.$city_id.'&c_name='.$company_name.'>トップページ</a>|<a href=?action=search&page='.($page-1).'&city_id='.$city_id.'&c_name='.$company_name.'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'&city_id='.$city_id.'&c_name='.$company_name.'>次頁</a>|<a href=?action=search&page='.$page_count.'&city_id='.$city_id.'&c_name='.$company_name.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				地域名:
			</div>
			<select name='city_id' id='city_id' style='margin-left:2px;'>
						<option value='' <?php if ($city_id=='') {echo 'selected';}?>></option>
				        <option value='1' <?php if ($city_id=='1') {echo 'selected';}?>>北海道</option>
                        <option value='2' <?php if ($city_id=='2') {echo 'selected';}?>>青森県</option>
                        <option value='3' <?php if ($city_id=='3') {echo 'selected';}?>>秋田県</option>
                        <option value='4' <?php if ($city_id=='4') {echo 'selected';}?>>岩手県</option>
                        <option value='5' <?php if ($city_id=='5') {echo 'selected';}?>>山形県</option>
                        <option value='6' <?php if ($city_id=='6') {echo 'selected';}?>>宮城県</option>
                        <option value='7' <?php if ($city_id=='7') {echo 'selected';}?>>福島県</option>
                        <option value='8' <?php if ($city_id=='8') {echo 'selected';}?>>新潟県</option>
                        <option value='9' <?php if ($city_id=='9') {echo 'selected';}?>>富山県</option>
                        <option value='10' <?php if ($city_id=='10') {echo 'selected';}?>>石川県</option>
                        <option value='11' <?php if ($city_id=='11') {echo 'selected';}?>>群馬県</option>
                        <option value='12' <?php if ($city_id=='12') {echo 'selected';}?>>栃木県</option>
                        <option value='13' <?php if ($city_id=='13') {echo 'selected';}?>>長野県</option>
                        <option value='14' <?php if ($city_id=='14') {echo 'selected';}?>>岐阜県</option>
                        <option value='15' <?php if ($city_id=='15') {echo 'selected';}?>>埼玉県</option>
                        <option value='16' <?php if ($city_id=='16') {echo 'selected';}?>>茨城県</option>
                        <option value='17' <?php if ($city_id=='17') {echo 'selected';}?>>東京都</option>
                        <option value='18' <?php if ($city_id=='18') {echo 'selected';}?>>千葉県</option>
                        <option value='19' <?php if ($city_id=='19') {echo 'selected';}?>>神奈川県</option>
                        <option value='20' <?php if ($city_id=='20') {echo 'selected';}?>>静岡県</option>
                        <option value='21' <?php if ($city_id=='21') {echo 'selected';}?>>山梨県</option>
                        <option value='22' <?php if ($city_id=='22') {echo 'selected';}?>>愛知県</option>
                        <option value='23' <?php if ($city_id=='23') {echo 'selected';}?>>福井県</option>
                        <option value='24' <?php if ($city_id=='24') {echo 'selected';}?>>滋賀県</option>
                        <option value='25' <?php if ($city_id=='25') {echo 'selected';}?>>三重県</option>
                        <option value='26' <?php if ($city_id=='26') {echo 'selected';}?>>京都府</option>
                        <option value='27' <?php if ($city_id=='27') {echo 'selected';}?>>奈良県</option>
                        <option value='28' <?php if ($city_id=='28') {echo 'selected';}?>>兵庫県</option>
                        <option value='29' <?php if ($city_id=='29') {echo 'selected';}?>>大阪府</option>
                        <option value='30' <?php if ($city_id=='30') {echo 'selected';}?>>和歌山県</option>
                        <option value='31' <?php if ($city_id=='31') {echo 'selected';}?>>島根県</option>
                        <option value='32' <?php if ($city_id=='32') {echo 'selected';}?>>鳥取県</option>
                        <option value='33' <?php if ($city_id=='33') {echo 'selected';}?>>岡山県</option>
                        <option value='34' <?php if ($city_id=='34') {echo 'selected';}?>>広島県</option>
                        <option value='35' <?php if ($city_id=='35') {echo 'selected';}?>>山口県</option>
                        <option value='36' <?php if ($city_id=='36') {echo 'selected';}?>>香川県</option>
                        <option value='37' <?php if ($city_id=='37') {echo 'selected';}?>>愛媛県</option>
                        <option value='38' <?php if ($city_id=='38') {echo 'selected';}?>>徳島県</option>
                        <option value='39' <?php if ($city_id=='39') {echo 'selected';}?>>高知県</option>
                        <option value='40' <?php if ($city_id=='40') {echo 'selected';}?>>大分県</option>
                        <option value='41' <?php if ($city_id=='41') {echo 'selected';}?>>福岡県</option>
                        <option value='42' <?php if ($city_id=='42') {echo 'selected';}?>>佐賀県</option>
                        <option value='43' <?php if ($city_id=='43') {echo 'selected';}?>>熊本県</option>
                        <option value='44' <?php if ($city_id=='44') {echo 'selected';}?>>長崎県</option>
                        <option value='45' <?php if ($city_id=='45') {echo 'selected';}?>>宮崎県</option>
                        <option value='46' <?php if ($city_id=='46') {echo 'selected';}?>>鹿児島県</option>
                        <option value='47' <?php if ($city_id=='47') {echo 'selected';}?>>沖縄県</option>
			</select>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
				企業名:
			</div>
			<div style='float:left; text-align:left;margin:2px; width:500px;height:20px;' >
                <input type='text' name='company_name' id='company_name' value='<?php echo$company_name?>' style='width:500px;' />
			</div>
			<div style='float:left; text-align:left;margin-left:60px;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='float:left; text-align:left;margin-left:40px;' >
				<input type="button" class="btn_search" value="会社追加" onclick="addCompany()" href="javascript:void(0)""/>
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
		<?php
			if ($rowCnt>0){
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr bgcolor='#DBE6F5'>
							<th width='60px'>操作</th>
							<th width='60px'>ID</th>
							<th width='120px'>地域名</th>
							<th width='550px'>企業名</th>
							<th width='200px'>記入時間</th>
						</tr>
					</table>
				";
				$i=1;

                while($arr_area_row=mysql_fetch_array($result_area)){
                    $arr_area_id[] = $arr_area_row;
                }
				 while($rs=mysql_fetch_object($result))
				{
				  echo "
					   <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='60px'align='center' >
									<input type='button' class='btn2' value='更新' onclick=\"updateChange("."'up_company_name".$i."',"."'u_area_id".$i."',".$rs->company_id.",".$page.")\">
								</td>
					";
				  echo "
								<td width='60px'align='center'>".$rs->company_id."</td>
				  ";
				  echo"
				        <td width='120px'align='center'>
                        <select name='u_area_id".$i."' id='u_area_id".$i."' style='margin-left:2px;' />
                            <option value=''></option>
                   ";
                            foreach($arr_area_id as $c_id=>$c_name){
                                if($c_name[area_id]==$rs->city_id){
                                    echo"<option value=".$c_name[area_id]." selected >".$c_name[area_name]. "</option>";
                                }else{
                                    echo"<option value=".$c_name[area_id]." >".$c_name[area_name]. "</option>";
                                }
                            }
                  echo"
                        </select>
                        </td>
                   ";
				   echo "
								<td width='550px'><input type='text' name='up_company_name".$i."' id='up_company_name".$i."' value=".$rs->company_name." style='width:546px;' ></input></td>
								<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
					";
				  echo "
							</tr>
					  </table>
					  </td>
					  </tr>
					</table>
					";
					$i++;
				}
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "検索結果がありません。";
				}
			}
		?>
		</form>
	<script language="javascript" type="text/javascript">
		function deleteInfo(c_id,page) {
			  var pageurl="?action=delete&c_id="+c_id+"&page="+page;
			  window.location.href=pageurl;
		}
		function show(msg,id) {
			document.getElementById(id).value=msg;
		}
		function initSearch(){
			document.form1.action='?action=search';
			document.form1.submit();
		}
		function updateChange(up_company_name,u_area_id,c_id,page) {
			var company_name=document.getElementById(up_company_name).value;
            var area_id=document.getElementById(u_area_id).value;
			var pageurl="?action=update&c_id="+c_id+"&name="+company_name+"&area_id="+area_id+"&page="+page;
			window.location.href=pageurl;
		}
	</script>
	</div>
	<div class="clearboth"></div>
</div>
<div id="addCompanyForm" style="text-align: center; display: none;">
    <form onsubmit="return false;" style="margin-bottom:0px;" method="post" action="add_company_info.php" name="addCompanyForm">
        <table border="0" style="font-size:12px; text-align:center; margin:30px auto;">
            <tbody>
            	<tr>
            		<td colspan="3" class="pirobox_up"><div class="piro_close" style="visibility: visible; display: block;"onclick="jQuery.unblockUI();initSearch();"></div></td>
            	</tr>
                <tr>
                    <td align="left" colspan="2">
                        <div style="font-size:20px; text-align:left; font-weight:bold; color:#900;">
                            企業情報追加
                        </div>
                        <div style='clear:both;'></div><br/>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
	                        <div style="color:#FC8B05; font-weight:bold;font-size:16px;width:100px;">
								地域名:
							</div>
							<select name='add_city_id' id='add_city_id' style='margin-left:2px;'>
										<option value='' <?php if ($city_id=='') {echo 'selected';}?>></option>
								        <option value='1' <?php if ($city_id=='1') {echo 'selected';}?>>北海道</option>
				                        <option value='2' <?php if ($city_id=='2') {echo 'selected';}?>>青森県</option>
				                        <option value='3' <?php if ($city_id=='3') {echo 'selected';}?>>秋田県</option>
				                        <option value='4' <?php if ($city_id=='4') {echo 'selected';}?>>岩手県</option>
				                        <option value='5' <?php if ($city_id=='5') {echo 'selected';}?>>山形県</option>
				                        <option value='6' <?php if ($city_id=='6') {echo 'selected';}?>>宮城県</option>
				                        <option value='7' <?php if ($city_id=='7') {echo 'selected';}?>>福島県</option>
				                        <option value='8' <?php if ($city_id=='8') {echo 'selected';}?>>新潟県</option>
				                        <option value='9' <?php if ($city_id=='9') {echo 'selected';}?>>富山県</option>
				                        <option value='10' <?php if ($city_id=='10') {echo 'selected';}?>>石川県</option>
				                        <option value='11' <?php if ($city_id=='11') {echo 'selected';}?>>群馬県</option>
				                        <option value='12' <?php if ($city_id=='12') {echo 'selected';}?>>栃木県</option>
				                        <option value='13' <?php if ($city_id=='13') {echo 'selected';}?>>長野県</option>
				                        <option value='15' <?php if ($city_id=='15') {echo 'selected';}?>>埼玉県</option>
				                        <option value='16' <?php if ($city_id=='16') {echo 'selected';}?>>茨城県</option>
				                        <option value='17' <?php if ($city_id=='17') {echo 'selected';}?>>東京都</option>
				                        <option value='18' <?php if ($city_id=='18') {echo 'selected';}?>>千葉県</option>
				                        <option value='19' <?php if ($city_id=='19') {echo 'selected';}?>>神奈川県</option>
				                        <option value='20' <?php if ($city_id=='20') {echo 'selected';}?>>静岡県</option>
				                        <option value='21' <?php if ($city_id=='21') {echo 'selected';}?>>山梨県</option>
				                        <option value='22' <?php if ($city_id=='22') {echo 'selected';}?>>愛知県</option>
                                        <option value='14' <?php if ($city_id=='14') {echo 'selected';}?>>岐阜県</option>
				                        <option value='23' <?php if ($city_id=='23') {echo 'selected';}?>>福井県</option>
				                        <option value='24' <?php if ($city_id=='24') {echo 'selected';}?>>滋賀県</option>
				                        <option value='25' <?php if ($city_id=='25') {echo 'selected';}?>>三重県</option>
				                        <option value='26' <?php if ($city_id=='26') {echo 'selected';}?>>京都府</option>
				                        <option value='27' <?php if ($city_id=='27') {echo 'selected';}?>>奈良県</option>
				                        <option value='28' <?php if ($city_id=='28') {echo 'selected';}?>>兵庫県</option>
				                        <option value='29' <?php if ($city_id=='29') {echo 'selected';}?>>大阪府</option>
				                        <option value='30' <?php if ($city_id=='30') {echo 'selected';}?>>和歌山県</option>
				                        <option value='31' <?php if ($city_id=='31') {echo 'selected';}?>>島根県</option>
				                        <option value='32' <?php if ($city_id=='32') {echo 'selected';}?>>鳥取県</option>
				                        <option value='33' <?php if ($city_id=='33') {echo 'selected';}?>>岡山県</option>
				                        <option value='34' <?php if ($city_id=='34') {echo 'selected';}?>>広島県</option>
				                        <option value='35' <?php if ($city_id=='35') {echo 'selected';}?>>山口県</option>
				                        <option value='36' <?php if ($city_id=='36') {echo 'selected';}?>>香川県</option>
				                        <option value='37' <?php if ($city_id=='37') {echo 'selected';}?>>愛媛県</option>
				                        <option value='38' <?php if ($city_id=='38') {echo 'selected';}?>>徳島県</option>
				                        <option value='39' <?php if ($city_id=='39') {echo 'selected';}?>>高知県</option>
				                        <option value='40' <?php if ($city_id=='40') {echo 'selected';}?>>大分県</option>
				                        <option value='41' <?php if ($city_id=='41') {echo 'selected';}?>>福岡県</option>
				                        <option value='42' <?php if ($city_id=='42') {echo 'selected';}?>>佐賀県</option>
				                        <option value='43' <?php if ($city_id=='43') {echo 'selected';}?>>熊本県</option>
				                        <option value='44' <?php if ($city_id=='44') {echo 'selected';}?>>長崎県</option>
				                        <option value='45' <?php if ($city_id=='45') {echo 'selected';}?>>宮崎県</option>
				                        <option value='46' <?php if ($city_id=='46') {echo 'selected';}?>>鹿児島県</option>
				                        <option value='47' <?php if ($city_id=='47') {echo 'selected';}?>>沖縄県</option>
							</select>
							<div style='clear:both;'></div><br/>
                    </td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
	                        <div style="color:#FC8B05; font-weight:bold;font-size:16px;width:100px;">企業名：</div>
	                        <input id="add_company_name" type="text" class="nomaltext" name="add_company_name"/>
                       		<div style='clear:both;'></div><br/>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="2">
                        <input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('企業情報を追加します。よろしいですか？');if(ret)addCompanySubmit()" value="確認"/>
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
</body>
</html>