<?php

//include
require '../util/include.php';

$home_page_name='シス蔵管理メニュー';
$home_page_url=URL_PATH;
$f_page_name='お知らせ管理メニュー';
$f_page_url=URL_PATH.'m_notice.php';
$page_name='プッシュ通知管理画面';

$action = $_GET['action'];
$area_id='';
$area_dist='';
$sex='';
$age_from='';
$age_to='';

$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
if(!$db){
    die("connot connect:" . mysql_error());
}

$dns = mysql_select_db(DB_NAME,$db);

if(!$dns){
    die("connot use db:" . mysql_error());
}

mysql_set_charset('utf8');

$sqlall = "select * from app_area WHERE 1";

$result_list = mysql_query($sqlall,$db);

mysql_close($db);


//Search
if (($action=='search')){

    $link = db_conn();
    mysql_set_charset('utf8');

    $page_size=100;

    if( isset($_GET['page']) ){
        $page = intval( $_GET['page'] );
    }
    else{
        $page = 1;
    }
    $rowCnt = 0;

    $area_id = $_POST['area_id'];
    $area_dist = $_POST['area_dist'];
    $sex = $_POST['sex'];
    $age_from = $_POST['age_from'];
    $age_to = $_POST['age_to'];

    //All
    $sqlall = "select aph.*,
                    (select area_name from app_area aa where aa.area_id=aph.area_id) area_name
                from app_push_history aph WHERE 1";

    if($area_id!='') {
        $sqlall .= " and aph.area_id = $area_id";
    }

    $result = mysql_query($sqlall,$link) or die(mysql_error());

    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
    }
    $rowCntall=mysql_num_rows($result);

    //Select current all
    $sql = sprintf("%s order by aph.id desc  limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);

    $result = mysql_query($sql,$link);

    if(!$result){
        $rowCnt = -1;
        db_disConn($result, $link);
    }

    $rowCnt=mysql_num_rows($result);

    //paging
    if($rowCnt==0){
        $page_count = 0;
        db_disConn($result, $link);
    }
    else{
        if( $rowCntall<$page_size ){ $page_count = 1; }
        if( $rowCntall%$page_size ){
            $page_count = (int)($rowCntall / $page_size) + 1;
        }else{
            $page_count = $rowCntall / $page_size;
        }
    }
    $page_string = '';
    if (($page == 1)||($page_count == 1)){
        $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
    }
    else{
        $page_string .= '<a href=?action=search&page=1>トップページ</a>|<a href=?action=search&page='.($page-1).'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
    }
    if( ($page == $page_count) || ($page_count == 0) ){
        $page_string .= '次頁|最終ページ';
    }
    else{
        $page_string .= '<a href=?action=search&page='.($page+1).'>次頁</a>|<a href=?action=search&page='.$page_count.'>最終ページ</a>';
    }
}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
    <title><?php echo $page_name; ?></title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" >
    <meta http-equiv="content-style-type" content="text/css">
    <meta http-equiv="content-script-type" content="text/javascript">
    <link href="../css/common.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="../js/common.js"></script>
    <script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
    <script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
    <div id="header_content">
        <h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
    </div>
</div>
<div id="nav">
    <div id="nav_content">
        <a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
        <a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
        <?php echo $page_name; ?>
    </div>
</div>
<div class='content'>
    <div style='float:left;margin-top:120px;margin-bottom:20px'>
        <form action='?action=search' method='post' name='form1'>
            <div style='float:left; text-align:left;margin:2px; width:180px;height:20px;' >
                送信対象地域名:
            </div>
            <div style='float:left; text-align:left;margin:2px; width:300px;height:20px;' >
                <select name='area_id' style='margin-left:2px;' selected/>
                <option value=''></option>
                <?php
                while($arr_list_row=mysql_fetch_array($result_list)){
                    if($arr_list_row[area_id]==$area_id){
                        echo"<option value=".$arr_list_row[area_id]." selected >".$arr_list_row[area_name]. "</option>";
                    }else{
                        echo"<option value=".$arr_list_row[area_id]." >".$arr_list_row[area_name]. "</option>";
                    }
                }
                ?>
                </select>
            </div>
            <div style='float:left; text-align:left;margin-left:60px;' >
                <input type="submit" class="btn_search" value="履歴検索" />
            </div>
            <div style='float:left; text-align:left;margin-left:40px;' >
                <input type="button" class="btn_search" value="通知作成" onclick="addPush()" href="javascript:void(0)""/>
            </div>
            <div style='clear:both; margin-bottom:20px'></div>
            <?php
            if ($rowCnt>0){
                echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
                echo "
					<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr bgcolor='#DBE6F5'>
							<th width='200px'>通知日時</th>
							<th width='550px'>メッセージ内容</th>
							<th width='80px'>対象地域</th>
						</tr>
					</table>

				";
                $i=1;
                while($rs=mysql_fetch_object($result))
                {
                    echo "
                    <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr align='left' bgcolor='#EEF2F4'>
						";
                    echo "
								<td width='200px' align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
								<td width='550px'>".$rs->message."</td>
					";
                    echo "
                            <td width='80px'align='center'>".$rs->area_name."</td>
                        </tr>
					</table>
					";
                    $i++;
                }
                echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
                mysql_close($link);
            }else{
                if ($action=='search'){
                    echo "検索結果がありません。";
                }
            }
            ?>
        </form>
        <script language="javascript" type="text/javascript">
            function deleteInfo(c_id) {
                var pageurl="?action=delete&c_id="+c_id;
                window.location.href=pageurl;
            }
            function show(msg,id) {
                document.getElementById(id).value=msg;
            }
            function initSearch(){
                document.form1.action='?action=search';
                document.form1.submit();
            }
        </script>
    </div>
    <div class="clearboth"></div>
</div>
<div id="addPushForm" style="text-align: center; display: none;">
    <form onsubmit="return false;" style="margin-bottom:0px;" method="post" action="add_series_info.php" name="addseriesForm">
        <table border="0" style="font-size:12px; text-align:center; margin:30px auto;">
            <tbody>
            <tr>
                <td colspan="3" class="pirobox_up"><div class="piro_close" style="visibility: visible; display: block;"onclick="jQuery.unblockUI();initSearch();"></div></td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <div style="font-size:20px; text-align:left; font-weight:bold; color:#900;">
                        プッシュ通知作成
                    </div>
                    <div style='clear:both;'></div><br/>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <div style="color:#FC8B05; font-weight:bold;font-size:16px;width:100px;">通知内容：</div>
                    <input id="add_msg" type="text" class="nomaltext" name="add_msg" value=""/>
                    <div style='clear:both;'></div><br/>
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <div style="color:#FC8B05; font-weight:bold;font-size:16px;width:100px;">対象地域：</div>
                    <select name='add_area_id' id='add_area_id' style='margin-left:2px;'>
                        <option value='' ></option>
				        <option value='1' >北海道</option>
                        <option value='2' >東北</option>
                        <option value='3' >甲信越</option>
                        <option value='4' >北陸</option>
                        <option value='5' >関東</option>
                        <option value='6' >東海</option>
                        <option value='7' >近畿</option>
                        <option value='8' >山陰・山陽</option>
                        <option value='9' >四国</option>
                        <option value='10' >九州</option>
                        <option value='11' >沖縄</option>
                        <option value='12' >海外</option>
                    </select>
                    <div style='clear:both;'></div><br/>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="2">
                    <input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('プッシュ情報を追加します。よろしいですか？');if(ret)addPushInfoSubmit()" value="確認"/>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
</body>
</html>