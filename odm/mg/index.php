<?php
	//運営管理⇒速報！倒産情報！！スレッド管理画面
	//include
	require '../util/include.php';
	$home_page_name='シス蔵管理画面';
	$home_page_url=URL_PATH;
	$page_name='シス蔵管理画面';
?>

<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title>シス蔵管理画面</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">

</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div class="content">
	<div style='text-align:center;'>
		<div style='text-align:left;margin-top:96px;margin-bottom:20px'>
			<a href="m_mem_user.php">ユーザー管理画面へ</a><br/><br/>
			<a href="m_app.php">アプリ管理画面へ</a><br/><br/>
			<a href="m_notice.php">お知らせ管理画面へ</a><br/><br/>
		</div>
	</div>
</div>

</body>
</html>
