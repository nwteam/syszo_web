<?php

	//include
	require '../util/include.php';

	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='アプリ管理メニュー';
	$f_page_url=URL_PATH.'m_app.php';
	$page_name='シストモ_チャット管理画面';

	$action = $_GET['action'];

	//Delete
	if ($action=='delete'){
		$m_id = $_GET['m_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from app_member_chat WHERE id = %d",$m_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if (($action=='search')||($action=='delete')||($action=='update')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=10;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$s_user_id = $_POST['s_user_id'];
		$s_user_name = $_POST['s_user_name'];
		$s_type = $_POST['s_type'];
		$s_content_words = $_POST['s_content_words'];

		//All
		$sqlall = "select amc.id,
					amc.user_id,
					am.user_nick user_name,
					amc.content,
					amc.insert_time,
					amc.type,
					amc.f_id,
					(select user_nick from app_member amm where amm.user_id=amc.f_id) as friend_name
				from app_member_chat amc LEFT JOIN app_member am ON amc.user_id = am.user_id WHERE 1";

		if($s_user_id!='') {
			$sqlall .= " and amc.user_id = $s_user_id";
		}
		if($s_user_name!='') {
			$sqlall .= " and am.user_nick = '$s_user_name'";
		}
		if($s_type!='') {
			$sqlall .= " and amc.type=$s_type";
		}
		if($s_content_words!='') {
			$sqlall .= " and amc.content like '%$s_content_words%'";
		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		$sql = sprintf("%s order by amc.user_id desc,amc.id desc limit %d,%d",$sqlall,($page-1)*$page_size,$page_size);
		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		else{
		   $page_string .= '<a href=?action=search&page=1>トップページ</a>|<a href=?action=search&page='.($page-1).'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
		   $page_string .= '<a href=?action=search&page='.($page+1).'>次頁</a>|<a href=?action=search&page='.$page_count.'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/common.js"></script>
<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				ユーザーID:
			</div>
			<div style='float:left; text-align:left; width:300px;height:20px;' >
				<input type='text' name='s_user_id' id='s_user_id' style='width:300px;height:20px;' value='<?php echo $s_user_id;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				ユーザー名:
			</div>
			<div style='float:left; text-align:left; width:300px;height:20px;' >
				<input type='text' name='s_user_name' id='s_user_name' style='width:300px;height:20px;' value='<?php echo $s_user_name;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				チャット内容分類:
			</div>
			<select name='s_type' id='s_type' style='float:left;width:180px;'>
				 <option value='' <?php if ($s_type=='') {echo 'selected';}?>></option>
				 <option value='1' <?php if ($s_type=='1') {echo 'selected';}?>>メッセージ</option>
				 <option value='2' <?php if ($s_type=='2') {echo 'selected';}?>>発注書</option>
			</select>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				チャット内容:
			</div>
			<div style='float:left; text-align:left; width:300px;height:20px;' >
				<input type='text' name='s_content_words' id='s_content_words' style='width:300px;height:20px;' value='<?php echo $s_content_words;?>'/>
			</div>
			<div style='float:left; text-align:left;margin-left:60px;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
		<?php
			if ($rowCnt>0){
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr bgcolor='#DBE6F5'>
							<th width='60px'>操作</th>
							<th width='100px'>連番</th>
							<th width='100px'>ユーザーID</th>
							<th width='200px'>ユーザー名</th>
							<th width='100px'>チャット対象ID</th>
							<th width='200px'>チャット対象名</th>
							<th width='100px'>チャット分類</th>
							<th width='550px'>チャット内容</th>
							<th width='200px'>チャット時間</th>
						</tr>
					</table>
				";
				$i=1;
				 while($rs=mysql_fetch_object($result))
				{
				  echo "
					   <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='60px'align='center'>
									<input type='button' class='btn3' value='削除' onclick=\"var ret=confirm('情報を削除します。よろしいですか？');if(ret)deleteInfo('".$rs->id."',".$page.")\">
								</td>
					";
				  echo "
								<td width='100px'align='center'>".$rs->id."</td>
								<td width='100px'align='center'>".$rs->user_id."</td>
								<td width='200px'>".$rs->user_name."</td>
								<td width='100px'align='center'>".$rs->f_id."</td>
								<td width='200px'>".$rs->friend_name."</td>
					";
				if ($rs->type=='0'){
				  	echo "
								<td width='100px'align='center'>メッセージ</td>
					";
				}
				elseif ($rs->type=='2'){
				  	echo "
								<td width='100px'align='center'>発注書</td>
					";
				}else{
					echo "
								<td width='100px'align='center'></td>
					";
				}
				echo "
								<td width='550px'>".$rs->content."</td>
								<td width='200px'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
							</tr>
					  </table>
					";
					$i++;
				}
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "検索結果がありません。";
				}
			}
		?>
		</form>
	<script language="javascript" type="text/javascript">
		function deleteInfo(m_id,page) {
			  var pageurl="?action=delete&m_id="+m_id+"&page="+page;
			  window.location.href=pageurl;
		}
	</script>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>