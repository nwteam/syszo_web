<?php
	//ユーザー管理⇒会員登録情報
	//include
	require '../util/include.php';
header('Cache-control: private, must-revalidate');
	$home_page_name='シス蔵管理メニュー';
	$home_page_url=URL_PATH;
	$f_page_name='ユーザー管理メニュー';
	$f_page_url=URL_PATH.'m_mem.php';
	$page_name='会員登録情報管理画面';

	$action = $_GET['action'];
	$up_user_id = $_GET['id'];
	$up_user_sts = $_GET['s'];
    $up_balance = $_GET['balance'];

	//Update
	if ($action=='update'){

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("UPDATE app_member SET status =%d,balance=%d WHERE user_id = %d",$up_user_sts,$up_balance    ,$up_user_id);
		$result = mysql_query($sql,$db);



		mysql_close($db);

	}
//Delete
	if ($action=='delete'){
		$user_id = $_GET['user_id'];

		$db = mysql_connect(DB_HOST,DB_USER,DB_PASS,DB_NAME);
		if(!$db){
			die("connot connect:" . mysql_error());
		}

		$dns = mysql_select_db(DB_NAME,$db);

		if(!$dns){
			die("connot use db:" . mysql_error());
		}

		mysql_set_charset('utf8');

		$sql = sprintf("delete from app_member WHERE user_id = %d",$user_id);
		$result = mysql_query($sql,$db);

		mysql_close($db);

	}
	//Search
	if (($action=='search')||($action=='update')||($action=='delete')){

		$link = db_conn();
		mysql_set_charset('utf8');

		$page_size=100;

		if( isset($_GET['page']) ){
		   $page = intval( $_GET['page'] );
		}
		else{
		   $page = 1;
		}
		$rowCnt = 0;

		$user_nick = $_POST['user_nick'];
		if($_GET['user_nick']!='') {
			$user_nick=$_GET['user_nick'];
		}
		$user_email = $_POST['user_email'];
		if($_GET['user_email']!='') {
			$user_email=$_GET['user_email'];
		}
		$user_sex = $_POST['user_sex'];
		if($_GET['user_sex']!='') {
			$user_sex=$_GET['user_sex'];
		}
		$YYYY = $_POST['YYYY'];
		if($_GET['YYYY']!='') {
			$YYYY=$_GET['YYYY'];
		}
		$MM = $_POST['MM'];
		if($_GET['MM']!='') {
			$MM=$_GET['MM'];
		}
		$DD = $_POST['DD'];
		if($_GET['DD']!='') {
			$DD=$_GET['DD'];
		}
		$YYYY_TO = $_POST['YYYY_TO'];
		if($_GET['YYYY_TO']!='') {
			$YYYY_TO=$_GET['YYYY_TO'];
		}
		$MM_TO = $_POST['MM_TO'];
		if($_GET['MM_TO']!='') {
			$MM_TO=$_GET['MM_TO'];
		}
		$DD_TO = $_POST['DD_TO'];
		if($_GET['DD_TO']!='') {
			$DD_TO=$_GET['DD_TO'];
		}
		$address_id = $_POST['address_id'];
		if($_GET['address_id']!='') {
			$address_id=$_GET['address_id'];
		}
		$user_id = $_POST['user_id'];
		if($_GET['user_id']!='') {
			$user_id=$_GET['user_id'];
		}
		$sort_item = $_POST['sort_item'];
		if($_GET['sort_item']!='') {
			$sort_item=$_GET['sort_item'];
		}
		$sort_type = $_POST['sort_type'];
		if($_GET['sort_type']!='') {
			$sort_type=$_GET['sort_type'];
		}

		//All
		$sqlall = "select am.*,aa.area_name,aj.name job_name,ai.industry_name,ascc.size_company_name,au.time_name,
					aq.qualified_name,asw.software_name,ah.hardware_name,aay.allyear_name,arm.reg_money_name,
					(select count(*) from app_fans af where am.user_id=af.uid_a) follow_cnt,
					(select count(*) from app_fans_b afb where am.user_id=afb.uid_b) follower_cnt 
					 from app_member am 
					 left join app_area aa on am.address=aa.area_id 
					 left join app_industry ai on am.industry=ai.industry_id 
					 left join app_jobs aj on am.job=aj.job_id 
					 left join app_size_company ascc on am.size_company=ascc.size_company_id 
					 left join app_use au on am.calendar=au.use_id 
					 left join app_software asw on am.software=asw.software_id 
					 left join app_hardware ah on am.hardware=ah.hardware_id 
					 left join app_qualified aq on am.qualified=aq.qualified_id 
					 left join app_allyear aay on am.allyear=aay.allyear_id 
					 left join app_reg_money arm on am.reg_money=arm.reg_money_id 
					 WHERE 1";

		//ニックネーム
		if($user_nick!='') {
			$sqlall .= " and user_nick like '%$user_nick%'";
		}
		//メールアドレス
		if($user_email!='') {
			$sqlall .= " and user_email like '%$user_email%'";
		}
		//会員状況
//		if($status!='') {
//			$sqlall .= " and status =$status";
//		}
		//性別
		if($user_sex!='') {
			$sqlall .= " and user_sex = $user_sex";
		}
		//登録年月日From
		$MM=sprintf("%02d",$MM);
		$DD=sprintf("%02d",$DD);
		$insert_time_from=$YYYY.'-'.$MM.'-'.$DD.' 00:00:00';
		$insert_time_from=strtotime($insert_time_from);

		if($YYYY!='') {
			$sqlall .= " and insert_time >= $insert_time_from ";
		}
		//登録年月日To
		$MM_TO=sprintf("%02d",$MM_TO);
		$DD_TO=sprintf("%02d",$DD_TO);
		$insert_time_to=$YYYY_TO.'-'.$MM_TO.'-'.$DD_TO.' 23:59:59';
		$insert_time_to=strtotime($insert_time_to);
		if($YYYY_TO!='') {
			$sqlall .= " and insert_time <= $insert_time_to ";
		}
		//居住都道府県
		if($address_id!='') {
			$sqlall .= " and address = $address_id";
		}
		//市区町村
//		if($distrcit!='') {
//			$sqlall .= " and distrcit like '%$distrcit%'";
//		}
		//ユーザーID
		if($user_id!='') {
			$sqlall .= " and user_id = $user_id";
		}
		//血液型
//		if($user_blood!='') {
//			$sqlall .= " and user_blood = $user_blood";
//		}
		//健康診断
//		if($user_health!='') {
//			$sqlall .= " and user_health = $user_health";
//		}

		$result = mysql_query($sqlall,$link) or die(mysql_error());

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}
		$rowCntall=mysql_num_rows($result);

		//Select current all
		//ソート順タイプ
		if($sort_type=='1'){
			$sort_str='desc';
		}
		elseif($sort_type=='2'){
			$sort_str='asc';
		}else{
			$sort_str='desc';
		}
		//ソート順
		if($sort_item=='1'){
			$sort_key='user_id';
		}
		elseif($sort_item=='2'){
			$sort_key='follow_cnt';
		}
		elseif($sort_item=='3'){
			$sort_key='follower_cnt';
		}else{
			$sort_key='user_id';
		}

		$sql = sprintf("%s order by %s %s limit %d,%d",$sqlall,$sort_key,$sort_str,($page-1)*$page_size,$page_size);

		$result = mysql_query($sql,$link);

		if(!$result){
			$rowCnt = -1;
			db_disConn($result, $link);
		}

		$rowCnt=mysql_num_rows($result);

		//paging
		if($rowCnt==0){
			$page_count = 0;
			db_disConn($result, $link);
		}
		else{
			if( $rowCntall<$page_size ){ $page_count = 1; }
			if( $rowCntall%$page_size ){
				$page_count = (int)($rowCntall / $page_size) + 1;
			}else{
				$page_count = $rowCntall / $page_size;
			}
		}
		$page_string = '';
		if (($page == 1)||($page_count == 1)){
		   $page_string .= 'トップページ|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
		}
		else{
			$page_string .= '<a href=?action=search&page=1'.
							'&user_nick='.$user_nick.
							'&user_email='.$user_email.
							'&address_id='.$address_id.
							'&user_sex='.$user_sex.
							'&user_id='.$user_id.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>トップページ</a>|<a href=?action=search&page='.($page-1).
							'&user_nick='.$user_nick.
							'&user_email='.$user_email.
							'&address_id='.$address_id.
							'&user_sex='.$user_sex.
							'&user_id='.$user_id.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>前頁</a>|第<b>'.($page).'</b>頁|計<b>'.($page_count).'</b>頁(<b>'.$rowCntall.'</b>件)|';
		}
		if( ($page == $page_count) || ($page_count == 0) ){
		   $page_string .= '次頁|最終ページ';
		}
		else{
			$page_string .= '<a href=?action=search&page='.($page+1).
							'&user_nick='.$user_nick.
							'&user_email='.$user_email.
							'&address_id='.$address_id.
							'&user_sex='.$user_sex.
							'&user_id='.$user_id.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>次頁</a>|<a href=?action=search&page='.$page_count.
							'&user_nick='.$user_nick.
							'&user_email='.$user_email.
							'&address_id='.$address_id.
							'&user_sex='.$user_sex.
							'&user_id='.$user_id.
							'&YYYY='.$YYYY.
							'&MM='.$MM.
							'&DD='.$DD.
							'&YYYY_TO='.$YYYY_TO.
							'&MM_TO='.$MM_TO.
							'&DD_TO='.$DD_TO.
							'&sort_item='.$sort_item.
							'&sort_type='.$sort_type.
							'>最終ページ</a>';
		}
	}



?>
<!Doctype html>
<html xmlns=http://www.w3.org/1999/xhtml>
<head>
<title><?php echo $page_name; ?></title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" >
<meta http-equiv="content-style-type" content="text/css">
<meta http-equiv="content-script-type" content="text/javascript">
<link href="../css/common.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="../js/year.js"></script>
<script type="text/javascript" src="../js/common.js"></script>
<script charset="utf-8" src="../js/jquery.js" type="text/javascript"></script>
<script src="../js/jquery.blockUI.js" type="text/javascript"></script>
</head>
<body>
<div id="header">
	<div id="header_content">
		<h1><a href="<?php echo $home_page_url; ?>">シス蔵管理画面</a></h1>
	</div>
</div>
<div id="nav">
	<div id="nav_content">
		<a href="<?php echo $home_page_url; ?>"><?php echo $home_page_name.' ＞ '; ?></a>
		<a href="<?php echo $f_page_url; ?>"><?php echo $f_page_name.' ＞ '; ?></a>
		<?php echo $page_name; ?>
	</div>
</div>
<div class='content'>
	<div style='float:left;margin-top:120px;margin-bottom:20px'>
		<form action='?action=search' method='post' name='form1'>
			<div style='float:left; text-align:left;width:180px;height:20px;' >
				ニックネーム:
			</div>
			<div style='float:left; text-align:left; width:296px;height:20px;' >
				<input type='text' name='user_nick' id='user_nick' style='width:296px;height:20px;' value='<?php echo $user_nick;?>'/>
			</div>
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:60px;' >
				メールアドレス:
			</div>
			<div style='float:left; text-align:left;width:296px;height:20px;' >
				<input type='text' name='user_email' id='user_email' style='width:296px;height:20px;'value='<?php echo $user_email;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				現在地:
			</div>
			<select name='address_id' id='address_id' style='float:left;width:180px;'>
						<option value='' <?php if ($address_id=='') {echo 'selected';}?>></option>
				        <option value='1' <?php if ($address_id=='1') {echo 'selected';}?>>北海道</option>
                        <option value='2' <?php if ($address_id=='2') {echo 'selected';}?>>東北</option>
                        <option value='3' <?php if ($address_id=='3') {echo 'selected';}?>>甲信越</option>
                        <option value='4' <?php if ($address_id=='4') {echo 'selected';}?>>北陸</option>
                        <option value='5' <?php if ($address_id=='5') {echo 'selected';}?>>関東</option>
                        <option value='6' <?php if ($address_id=='6') {echo 'selected';}?>>東海</option>
                        <option value='7' <?php if ($address_id=='7') {echo 'selected';}?>>近畿</option>
                        <option value='8' <?php if ($address_id=='8') {echo 'selected';}?>>山陰・山陽</option>
                        <option value='9' <?php if ($address_id=='9') {echo 'selected';}?>>四国</option>
                        <option value='10' <?php if ($address_id=='10') {echo 'selected';}?>>九州</option>
                        <option value='11' <?php if ($address_id=='11') {echo 'selected';}?>>沖縄</option>
                        <option value='12' <?php if ($address_id=='12') {echo 'selected';}?>>海外</option>
			</select>
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:176px;' >
				性別:
			</div>
			<select name='user_sex'  id='user_sex' style='float:left;width:180px;'>
				 <option value='' <?php if ($user_sex=='') {echo 'selected';}?>></option>
				 <option value='1' <?php if ($user_sex=='1') {echo 'selected';}?>>1：男性</option>
				 <option value='2' <?php if ($user_sex=='2') {echo 'selected';}?>>2：女性</option>
			</select>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				ユーザーID:
			</div>
			<div style='float:left; text-align:left; width:296px;height:25px;' >
				<input type='text' name='user_id' id='user_id' style='width:296px;height:20px;' value='<?php echo $user_id;?>'/>
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				申込年月日From:
			</div>
			<div style='float:left; text-align:left;height:20px;' >
				<select name='YYYY' id='YYYY' onchange="YYYYDD(this.value)">
					<option value="">年を選択</option>
				</select>年
				<select name='MM' id='MM' onchange="MMDD(this.value)">
					<option value="">月を選択</option>
				</select>月
				<select name='DD'>
					<option value="">日を選択</option>
				</select>日
			</div>
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:60px;' >
				ソート順:
			</div>
			<div style='float:left; text-align:left; width:400px;height:25px;' >
				<input type="radio" name="sort_item" value="1" <?php if ($sort_key=='user_id') {echo 'checked=checked';}?>/>申込日付
				<input type="radio" name="sort_item" value="2" <?php if ($sort_key=='follow_cnt') {echo 'checked=checked';}?>/>フォロー数
				<input type="radio" name="sort_item" value="3" <?php if ($sort_key=='follower_cnt') {echo 'checked=checked';}?>/>フォロワー数
			</div>
			<div style='clear:both;'></div><br/>
			<div style='float:left; text-align:left; width:180px;height:20px;' >
				申込年月日To:
			</div>
			<div style='float:left; text-align:left;height:20px;' >
				<select name='YYYY_TO' id='YYYY_TO' onchange="YYYYDD(this.value)">
					<option value="">年を選択</option>
				</select>年
				<select name='MM_TO' id='MM_TO' onchange="MMDDTO(this.value)">
					<option value="">月を選択</option>
				</select>月
				<select name='DD_TO'>
					<option value="">日を選択</option>
				</select>日
			</div>
			<div style='float:left; text-align:left; width:180px;height:20px;margin-left:60px;' >
				ソート順タイプ:
			</div>
			<div style='float:left; text-align:left; width:296px;height:25px;' >
				<input type="radio" name="sort_type" value="1" <?php if ($sort_str=='desc') {echo 'checked=checked';}?>/>降順
				<input type="radio" name="sort_type" value="2" <?php if ($sort_str=='asc') {echo 'checked=checked';}?>/>昇順
			</div>
			<div style='float:left; text-align:left;margin-left:60px;' >
				<input type="submit" class="btn_search" value="検索" />
			</div>
			<div style='float:left; text-align:left;margin-left:40px;' >
					<input type="button" class="btn_search" value="ユーザー作成" onclick="addUser()" href="javascript:void(0)""/>
			</div>
			<div style='clear:both; margin-bottom:20px'></div>
		<?php
			if ($rowCnt>0){
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				echo "
					<table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
						<tr bgcolor='#DBE6F5'>
							<th width='60px'>操作</th>
							<th width='200px'>デバイス</th>
							<th width='255px'>ニックネーム</th>
							<th width='255px'>メールアドレス</th>
							<th width='100px'>フォロー数</th>
							<th width='100px'>フォロワー数</th>
							<th width='80px'>性別</th>
							<th width='120px'>現在地</th>
							<th width='200px'>業種</th>
							<th width='200px'>会社規模</th>
							<th width='200px'>役職・役割</th>
							<th width='200px'>予算規模</th>
							<th width='200px'>情シス歴</th>
							<th width='200px'>ソフトウェア</th>
							<th width='200px'>ハードウェア</th>
							<th width='200px'>保有資格</th>
							<th width='200px'>年間予算</th>
							<th width='200px'>申込時間</th>
							<th width='200px'>最終ログイン</th>
							<th width='200px'>最終書込</th>
							<th width='200px'>ユーザーID</th>

						</tr>
					</table>
				";
				$i=1;
				 while($rs=mysql_fetch_object($result))
				{
				  echo "
					   <table width='98%' cellspacing='5' cellpadding='2' style='table-layout:fixed;'>
							<tr align='left' bgcolor='#EEF2F4'>
								<td width='60px'align='center'>
									<input type='button' class='btn3' value='削除' onclick=\"var ret=confirm('該当ユーザを削除します。よろしいですか？');if(ret)deleteInfo('".$rs->user_id."',".$page.")\">
								</td>
					";
				//デバイス
				  if ($rs->device=='1'){
					  echo "
							<td width='200px'align='center'>Android</td>
						";
				  }
				  elseif ($rs->device=='2'){
					  echo "
							<td width='200px'align='center'>iOS</td>
						";
				  }
				  else{
					  echo "
							<td width='200px'align='center'>Unknown</td>
						";
				  }
				    echo "
							<td width='255px'align='center'>".$rs->user_nick."</td>
							<td width='255px'align='center'>".$rs->user_email."</td>
				";
				  if ($rs->follow_cnt==0){
					  echo "
							<td width='100px'align='center'>$rs->follow_cnt</td>
					  ";
				  }else{
					  echo "
							<td width='100px'align='center'><a href='javascript:void(0)'onclick='searchFollow($rs->user_id)' >".$rs->follow_cnt."</a></td>
					  ";
				  }
				  if ($rs->follower_cnt==0){
					  echo "
							<td width='100px'align='center'>$rs->follower_cnt</td>
					  ";
				  }else{
					  echo "
							<td width='100px'align='center'><a href='javascript:void(0)'onclick='searchFollower($rs->user_id)' >".$rs->follower_cnt."</a></td>
					  ";
				  }
				  //性別
				  if ($rs->user_sex==1){
					  echo "
							<td width='80px' align='center'>1:男性</td>
						";
				  }
				  elseif ($rs->user_sex==2){
					  echo "
							<td width='80px' align='center'>2:女性</td>
						";
				  }
				   else{
					  echo "
							<td width='80px'align='center'></td>
						";
				  }
				  echo "
							<td width='120px'align='center'>".$rs->area_name."</td>
							<td width='200px'align='center'>".$rs->industry_name."</td>
							<td width='200px'align='center'>".$rs->size_company_name."</td>
							<td width='200px'align='center'>".$rs->job_name."</td>
							<td width='200px'align='center'>".$rs->allyear_name."</td>
							<td width='200px'align='center'>".$rs->time_name."</td>
							<td width='200px'align='center'>".$rs->software_name."</td>
							<td width='200px'align='center'>".$rs->hardware_name."</td>
							<td width='200px'align='center'>".$rs->qualified_name."</td>
							<td width='200px'align='center'>".$rs->reg_money_name."</td>
				";
				  echo "
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->insert_time)."</td>
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->last_login)."</td>
							<td width='200px'align='center'>".date("Y-m-d H:i:s",$rs->last_input)."</td>
							<td width='200px'align='center'>".$rs->user_id."</td>
				";
				 
				  echo"
							</tr>
					  </table>
					";
					$i++;
				}
				echo "
					<table width='100%' cellspacing='1' cellpadding='2'>
						<tr bgcolor='#DBE6F5'>
						  <td><span style='float:left; text-align:center'><font color=#666666>$page_string</font></span></td>
						</tr>
					</table>";
				mysql_close($link);
			}else{
				if ($action=='search'){
					echo "検索結果がありません。";
				}
			}
		?>
		</form>
	<script language="javascript" type="text/javascript">
		function deleteInfo(user_id,page) {
			  var pageurl="?action=delete&user_id="+user_id+"&page="+page;
			  window.location.href=pageurl;
		}
		function updateChange(user_id,t_sts,page,up_balance) {
			  var user_sts=document.getElementById(t_sts).value;
              var balance=document.getElementById(up_balance).value;
			  var pageurl="?action=update&id="+user_id+"&s="+user_sts+"&page="+page+"&balance="+balance;
			  window.location.href=pageurl;
		}

		function show(msg,id) {
			document.getElementById(id).value=msg;
		}
		function initSearch(){
			document.form1.action='?action=search';
			document.form1.submit();
		}
	</script>
	</div>
	<div id="addUserForm" style="text-align: center; display: none;">
			<form onsubmit="return false;" style="margin-bottom:0px;" method="post" action="add_user.php" name="addUserForm">
					<table border="0" style="font-size:12px; text-align:center; margin:30px auto;">
							<tbody>
							<tr>
									<td colspan="3" class="pirobox_up"><div class="piro_close" style="visibility: visible; display: block;"onclick="jQuery.unblockUI();initSearch();"></div></td>
							</tr>
							<tr>
									<td align="left" colspan="2">
											<div style="font-size:20px; text-align:left; font-weight:bold; color:#900;">
													ユーザー新規作成
											</div>
											<div style='clear:both;'></div><br/>
									</td>
							</tr>
							<tr>
									<td align="left" colspan="2">
											<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">ニックネーム：</div>
											<input id="add_user_nick" type="text" class="nomaltext" name="add_user_nick" value=""/>
											<div style='clear:both;'></div><br/>
									</td>
							</tr>
							<tr>
									<td align="left" colspan="2">
											<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">メールアドレス：</div>
											<input id="add_user_email" type="text" class="nomaltext" name="add_user_email" value=""/>
											<div style='clear:both;'></div><br/>
									</td>
							</tr>
							<tr>
									<td align="left" colspan="2">
											<div style="color:#FC8B05; font-weight:bold;font-size:16px;width:150px;">パスワード：</div>
											<input id="add_user_pwd" type="password" class="nomaltext" name="add_user_pwd" value=""/>
											<div style='clear:both;'></div><br/>
									</td>
							</tr>
							<tr>
									<td align="center" colspan="2">
											<input type="button" style="color:white;font-size:16px; line-height:16px;background-color:orange;width:100px;height:30px;font-weight: bold;"onclick="var ret=confirm('ユーザー情報を追加します。よろしいですか？');if(ret)addUserInfoSubmit()" value="確認"/>
									</td>
							</tr>
							</tbody>
					</table>
			</form>
	</div>
	<div class="clearboth"></div>
</div>
</body>
</html>