<header>
  <div class="inner">
    <h1><a href="http://syszo.com/"><img src="images/logo.png" alt="情シス特化型メディア SYSZO"/></a></h1>
    <div id="utility">
      <p>SYSZOは、情シスに関わるノウハウを記録・共有するサービスです。</p>
      <ul>
				<?php 
					if($_SESSION['user_id']!=""||$_COOKIE['user_id']!=""){
						echo "
							<li class='welcome'>ようこそ<span>$_SESSION[user_nick]$_COOKIE[user_nick]</span>さん</li>
							<li class='loggedin'><a href='https://syszo.com/mypage.php'>マイページ</a></li>
							<li class='logout'><a href='https://syszo.com/logout.php'>ログアウト</a></li>
							";
					}else{
						echo "
							<li class='membership'><a href='https://syszo.com/register.php'>ユーザー登録（無料・完全匿名）</a></li>
							<li class='logIn'><a href='https://syszo.com/login.php'>ログイン</a></li>
							";
					}
				?>
      </ul>
    </div>
  </div>
</header>