<!doctype html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="keywords" content="情シス、情報システム、シス蔵、SYSZO、社内システム、社内SE、システム管理">
<meta name="description" content="SYSZOは情シスに関わるノウハウを記録・共有するサービスです。みなさんの疑問・体験・想いを集めて情報システム部門の集合知を作ります。あなたの体験は別の誰かのためになる！">
<meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no">
<title><?php echo $title;?></title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"></script>
<link href="css/base.css" rel="stylesheet" type="text/css" media="all">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
<link rel="shortcut icon" href="images/favicon.ico">
<!--[if lt IE 9]>
<script src="https://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<script src="https://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<link rel="stylesheet" href="css/pc.css">
<![endif]-->
<!-- Facebook Pixel Code -->
<script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1091004367664914');
    fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1091004367664914&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->