<?php
	require 'include.php';
	$title="SYSZO - 情シス特化型メディア";

	$login_user_id=$_SESSION['user_id'];
	if($login_user_id==""){$login_user_id=$_COOKIE['user_id'];}
	$login_user_name=$_SESSION['user_nick'];
	if($login_user_name==""){$login_user_name=$_COOKIE['user_nick'];}

	if($login_user_id==""){header("Location:https://syszo.com/login.php");}

	$url = API_PATH.API_MYSELF_MYDATA;
	$post_data['user_id'] = $login_user_id;

	$o = "";
	foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data = substr($o,0,-1);
	$res = request_post($url, $post_data);
	$obj = json_decode($res);

	$result = $obj->{'result'};
	$msg = $obj->{'msg'};
	if($result!="0"){
		$user_nick=$obj->{'data'}->{'user_nick'};
		$user_email=$obj->{'data'}->{'user_email'};

	}
		

?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
    <h2><span class="userName"><?php echo $user_nick;?></span><span class="mini">さんのマイページ</span><span id="account"><a href="password_editing.php">パスワード変更</a></span><span id="editing"><a href="mypage.php">戻る</a></span></h2>
    <div id="yourPage">
      <dl>
        <dt>メールアドレス</dt>
        <dd><?php echo $user_email;?></dd>
      </dl>
      <dl>
        <dt>パスワード</dt>
        <dd>＊＊＊＊＊＊＊＊＊＊＊＊</dd>
      </dl>
      
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>