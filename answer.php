<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

$url = API_PATH.API_KNOW_INFO_LIST;

//最新のコメント順
$post_data_replay['p_size'] = 1000000;//表示件数
$post_data_replay['type'] = 2;//最新のコメント順

$o = "";
foreach ( $post_data_replay as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_replay = substr($o,0,-1);
$res = request_post($url, $post_data_replay);
$replay_json = json_decode($res,TRUE);
//echo var_dump($replay_json);

$result = $replay_json['result'];
$msg = $replay_json['msg'];
if($result!="0"){
	$count_replay = count($replay_json["data"]);
}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <div id="contents">
		<?php include "nav.php"; ?>
    <section id="answer">
      <h2>最新のコメント順</h2>
      <ul>
				<?php for ($i = 0; $i < $count_replay; $i++){ ?>
					<li <?php if($replay_json["data"][$i]['urgent']=="2") {echo "class=''";}?>>
						<a href="detail.php?id=<?php echo $replay_json["data"][$i]['know_id'];?>">
						<span class="postTitle"><?php echo $replay_json["data"][$i]["title"];?></span>
						<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($replay_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#newPosts--> 
    
  </div>
  <!--/#contents-->
	<?php include "side.php"; ?>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>