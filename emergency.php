<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

$url = API_PATH.API_KNOW_INFO_LIST;

//緊急トラブル発生中！
$post_data['p_size'] = 1000000;//表示件数
$post_data['type'] = 4;//トラブル発生中

$o = "";
foreach ( $post_data as $k => $v ){$o.= "$k=" . urlencode( $v ). "&" ;}
$post_data = substr($o,0,-1);
$res = request_post($url, $post_data);
$urgent_json = json_decode($res,TRUE);
//echo var_dump($urgent_json);

$result = $urgent_json['result'];
$msg = $urgent_json['msg'];
if($result!="0"){
	$count_urgent = count($urgent_json["data"]);
}
?>
<?php include "head.php"; ?>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <div id="contents">
		<?php include "nav.php"; ?>
    <section id="emergencyArea">
      <h2>緊急トラブル発生中！</h2>
      <ul>
				<?php for ($i = 0; $i < $count_urgent; $i++){ ?>
					<li <?php if($urgent_json["data"][$i]['urgent']=="2") {echo "class='emergency'";}?>>
						<a href="detail.php?id=<?php echo $urgent_json["data"][$i]['know_id'];?>">
						<span class="postTitle"><?php echo $urgent_json["data"][$i]["title"];?></span>
						<span class="date">update - <?php echo date('Y.m.d H:i',strtotime($urgent_json["data"][$i]['time']));?></span>
						</a>
					</li>
				<?php } ?>
      </ul>
    </section>
    <!--/#newPosts--> 
    
  </div>
  <!--/#contents-->
	<?php include "side.php"; ?>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>
</body>
</html>