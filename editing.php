<?php
require 'include.php';
$title="SYSZO - 情シス特化型メディア";

$login_user_id=$_SESSION['user_id'];
if($login_user_id==""){$login_user_id=$_COOKIE['user_id'];}
$login_user_name=$_SESSION['user_nick'];
if($login_user_name==""){$login_user_name=$_COOKIE['user_nick'];}

if($login_user_id==""){header("Location:https://syszo.com/login.php");}

$action=$_GET['action'];

$url_get = API_PATH.API_MYSELF_MYDATA;
$post_data_get['user_id'] = $login_user_id;

if ($action=="save"){
	$url_data_edit = API_PATH.API_MYSELF_MYDATA_EDIT;

	$user_email=$_POST['user_email'];
	$user_nick=$_POST['user_nick'];
	$user_sex=$_POST['user_sex'];
	$address=$_POST['address'];
	$industry=$_POST['industry'];
	$size_company=$_POST['size_company'];
	$job=$_POST['job'];
	$reg_money=$_POST['reg_money'];
	$calendar=$_POST['calendar'];
	//$software=$_POST['software'];
	$software=$_POST['h_software'];
	//$hardware=$_POST['hardware'];
	$hardware=$_POST['h_hardware'];
	//$qualified=$_POST['qualified'];
	$qualified=$_POST['h_qualified'];
	$allyear=$_POST['allyear'];
	$mail_flg=$_POST['mail_flg'];
	$introduction=$_POST['introduction'];

	$post_data_save['user_id'] = $login_user_id;
	$post_data_save['user_email']=$user_email;
	$post_data_save['user_nick']=$user_nick;
	$post_data_save['user_sex']=$user_sex;
	$post_data_save['address']=$address;
	$post_data_save['industry']=$industry;
	$post_data_save['size_company']=$size_company;
	$post_data_save['job']=$job;
	$post_data_save['reg_money']=$reg_money;
	$post_data_save['calendar']=$calendar;
	$post_data_save['software']=$software;
	$post_data_save['hardware']=$hardware;
	$post_data_save['qualified']=$qualified;
	$post_data_save['allyear']=$allyear;
	$post_data_save['mail_flg']=$mail_flg;
	$post_data_save['introduction']=$introduction;

	$o = "";
	foreach ( $post_data_save as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
	$post_data_save = substr($o,0,-1);
	$res_save = request_post($url_data_edit, $post_data_save);
	$obj_save = json_decode($res_save);

	$result_save = $obj_save->{'result'};
	$err_msg = $obj_save->{'msg'};
	if($result_save!="0"){
		header("Location:mypage.php");
	}
}

$o = "";
foreach ( $post_data_get as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}
$post_data_get = substr($o,0,-1);
$res_get = request_post($url_get, $post_data_get);
$obj = json_decode($res_get);

$result = $obj->{'result'};
$msg = $obj->{'msg'};
if($result!="0"){
	$user_email=$obj->{'data'}->{'user_email'};
	$user_nick=$obj->{'data'}->{'user_nick'};
	$user_sex=$obj->{'data'}->{'user_sex'};
	$address=$obj->{'data'}->{'address'};
	$industry=$obj->{'data'}->{'industry'};
	$size_company=$obj->{'data'}->{'size_company'};
	$job=$obj->{'data'}->{'job'};
	$reg_money=$obj->{'data'}->{'reg_money'};
	$calendar=$obj->{'data'}->{'calendar'};
	$software=$obj->{'data'}->{'software'};
	$arr_software=explode(",",$software);
	$hardware=$obj->{'data'}->{'hardware'};
	$arr_hardware=explode(",",$hardware);
	$qualified=$obj->{'data'}->{'qualified'};
	$arr_qualified=explode(",",$qualified);
	$allyear=$obj->{'data'}->{'allyear'};
	$number=$obj->{'data'}->{'number'};
	$price=$obj->{'data'}->{'price'};
	$time=$obj->{'data'}->{'time'};
	$introduction=$obj->{'data'}->{'introduction'};
	$scale=$obj->{'data'}->{'scale'};
	$mail_flg=$obj->{'data'}->{'mail_flg'};

}

$url_get_address = API_PATH.API_MYSELF_MYDATA_GET_ADDRESS;
$url_get_job = API_PATH.API_MYSELF_MYDATA_GET_JOB;
$url_get_use = API_PATH.API_MYSELF_MYDATA_GET_USE;
$url_get_industry = API_PATH.API_MYSELF_MYDATA_GET_INDUSTRY;
$url_get_size_company = API_PATH.API_MYSELF_MYDATA_GET_SIZE_COMPANY;
$url_get_reg_money = API_PATH.API_MYSELF_MYDATA_GET_REG_MONEY;
$url_get_software = API_PATH.API_MYSELF_MYDATA_GET_SOFTWARE;
$url_get_hardware = API_PATH.API_MYSELF_MYDATA_GET_HARDWARE;
$url_get_qualified = API_PATH.API_MYSELF_MYDATA_GET_QUALIFIED;
$url_get_allyear = API_PATH.API_MYSELF_MYDATA_GET_ALLYEAR;

$post_data['area_id'] = "1";

$o = "";
foreach ( $post_data as $k => $v ){ $o.= "$k=" . urlencode( $v ). "&" ;}$post_data = substr($o,0,-1);

$res1 = request_post($url_get_address, $post_data);$obj_address = json_decode($res1,TRUE);
$res2 = request_post($url_get_job, $post_data);$obj_job = json_decode($res2,TRUE);
$res3 = request_post($url_get_use, $post_data);$obj_use = json_decode($res3,TRUE);
$res4 = request_post($url_get_industry, $post_data);$obj_industry = json_decode($res4,TRUE);
$res5 = request_post($url_get_size_company, $post_data);$obj_size_company = json_decode($res5,TRUE);
$res6 = request_post($url_get_reg_money, $post_data);$obj_reg_money = json_decode($res6,TRUE);
$res7 = request_post($url_get_software, $post_data);$obj_software = json_decode($res7,TRUE);
$res8 = request_post($url_get_hardware, $post_data);$obj_hardware = json_decode($res8,TRUE);
$res9 = request_post($url_get_qualified, $post_data);$obj_qualified = json_decode($res9,TRUE);
$res10 = request_post($url_get_allyear, $post_data);$obj_allyear = json_decode($res10,TRUE);

$count_address = count($obj_address["data"]);
$count_job = count($obj_job["data"]);
$count_use = count($obj_use["data"]);
$count_industry = count($obj_industry["data"]);
$count_size_company = count($obj_size_company["data"]);
$count_reg_money = count($obj_reg_money["data"]);
$count_software = count($obj_software["data"]);
$count_hardware = count($obj_hardware["data"]);
$count_qualified = count($obj_qualified["data"]);
$count_allyear = count($obj_allyear["data"]);


?>
<?php include "head.php"; ?>
<link rel="stylesheet" href="css/chosen.css">

<link rel="stylesheet" type="text/css" href="js/multiselectSrc/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css" href="js/multiselectSrc/jquery.multiselect.filter.css" />
<link rel="stylesheet" type="text/css" href="js/assets/style.css" />
<link rel="stylesheet" type="text/css" href="js/assets/prettify.css" />
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />

<link href="css/base.css" rel="stylesheet" type="text/css" media="all">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all">
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/multiselectSrc/jquery.multiselect.js"></script>
<script type="text/javascript" src="js/multiselectSrc/jquery.multiselect.filter.js"></script>
<script type="text/javascript" src="js/assets/prettify.js"></script>

<script type="text/javascript">
$(function(){
	$("#software").multiselect({noneSelectedText: "",checkAllText: "全て選択",uncheckAllText: '全て選択解除',selectedText: '# 個以上',minWidth:174,multiple:true,selectedList:5}).multiselectfilter({label:"フィルタ",placeholder:""});
	$("#hardware").multiselect({noneSelectedText: "",checkAllText: "全て選択",uncheckAllText: '全て選択解除',selectedText: '# 個以上',minWidth:174,multiple:true,selectedList:5}).multiselectfilter({label:"フィルタ",placeholder:""});
	$("#qualified").multiselect({noneSelectedText: "",checkAllText: "全て選択",uncheckAllText: '全て選択解除',selectedText: '# 個以上',minWidth:174,multiple:true,selectedList:5}).multiselectfilter({label:"フィルタ",placeholder:""});

});
</script>
</head>
<body>
<?php include "header.php"; ?>
<div id="wrapper">
  <section id="mypage">
    <h2><span class="userName"><?php echo $user_nick;?></span><span class="mini">さんのマイページ</span><span id="editing"><a href="mypage.php">戻る</a></span></h2>
    <div id="yourPage">
      <form action="?action=save" method="post" name="upform">
				<?php if($result_save=="0"){echo "<dt  style='color:red;width:100%;'>$err_msg</dt>";}?>
				<dl>
					<dt>メールアドレス</dt>
					<dd><input name="user_email"  id="user_email" type="text" value="<?php if($user_email!=""){echo $user_email;}?>" size="40" /></dd>
				</dl>
				<dl>
					<dt>ニックネーム</dt>
					<dd><input name="user_nick"  id="user_nick" type="text" value="<?php if($user_nick!=""){echo $user_nick;}?>" size="40" /></dd>
				</dl>
        <dl>
          <dt>性別</dt>
          <dd>
            <select name="user_sex" id="user_sex">
              <option value="" <?php if($user_sex==""){echo "selected='selected'";}?>></option>
              <option value="1" <?php if($user_sex=="1"){echo "selected='selected'";}?>>男</option>
              <option value="2" <?php if($user_sex=="2"){echo "selected='selected'";}?>>女</option>
              <option value="3" <?php if($user_sex=="3"){echo "selected='selected'";}?>>その他</option>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>現在地</dt>
          <dd>
            <select name="address" id="address">
							<option value=""></option>
						<?php for ($i = 0; $i < $count_address; $i++){ ?>
							<option value="<?php echo $obj_address["data"][$i]["area_id"];?>" <?php if($address==$obj_address["data"][$i]["area_name"]){echo "selected='selected'";}?>><?php echo $obj_address["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>業種</dt>
          <dd>
            <select name="industry" id="industry">
              <option value=""></option>
						<?php for ($i = 0; $i < $count_industry; $i++){ ?>
							<option value="<?php echo $obj_industry["data"][$i]["area_id"];?>" <?php if($industry==$obj_industry["data"][$i]["area_name"]){echo "selected='selected'";}?>><?php echo $obj_industry["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>会社規模</dt>
          <dd>
            <select name="size_company" id="size_company">
              <option value=""></option>
						<?php for ($i = 0; $i < $count_size_company; $i++){ ?>
							<option value="<?php echo $obj_size_company["data"][$i]["area_id"];?>" <?php if($size_company==$obj_size_company["data"][$i]["area_name"]){echo "selected='selected'";}?>><?php echo $obj_size_company["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>役職・役割</dt>
          <dd>
            <select name="job" id="job">
              <option value=""></option>
						<?php for ($i = 0; $i < $count_job; $i++){ ?>
							<option value="<?php echo $obj_job["data"][$i]["area_id"];?>" <?php if($job==$obj_job["data"][$i]["area_name"]){echo "selected='selected'";}?>><?php echo $obj_job["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>年間予算</dt>
          <dd>
            <select name="reg_money" id="reg_money">
              <option value="" ></option>
						<?php for ($i = 0; $i < $count_reg_money; $i++){ ?>
							<option value="<?php echo $obj_reg_money["data"][$i]["area_id"];?>" <?php if($reg_money==$obj_reg_money["data"][$i]["area_name"]){echo "selected='selected'";}?>><?php echo $obj_reg_money["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>情シス歴</dt>
          <dd>
            <select name="calendar" id="calendar">
              <option value=""></option>
						<?php for ($i = 0; $i < $count_use; $i++){ ?>
							<option value="<?php echo $obj_use["data"][$i]["area_id"];?>" <?php if($calendar==$obj_use["data"][$i]["area_name"]){echo "selected='selected'";}?>><?php echo $obj_use["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
<!--
        <dl>
          <dt>ソフトウェア</dt>
          <dd>
            <select name="software" id="software" style="width:367px;" multiple='multiple' size='5'>
						<?php for ($i = 0; $i < $count_software; $i++){ ?>
							<option value="<?php echo $obj_software["data"][$i]["area_id"];?>" <?php for($j=0;$j<count($arr_software);$j++){if($arr_software[$j]==$obj_software["data"][$i]["area_name"]){echo "selected='selected'";}}?>><?php echo $obj_software["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>ハードウェア</dt>
          <dd>
            <select name="hardware" id="hardware"  style="width:367px;" multiple='multiple' size='5'>
						<?php for ($i = 0; $i < $count_hardware; $i++){ ?>
							<option value="<?php echo $obj_hardware["data"][$i]["area_id"];?>" <?php for($j=0;$j<count($arr_hardware);$j++){if($arr_hardware[$j]==$obj_hardware["data"][$i]["area_name"]){echo "selected='selected'";}}?>><?php echo $obj_hardware["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
        <dl>
          <dt>保有資格</dt>
          <dd>
						<select  name="qualified" id="qualified" style="width:367px;" multiple='multiple' size='5'>
							<?php for ($i = 0; $i < $count_qualified; $i++){ ?>
								<option value="<?php echo $obj_qualified["data"][$i]["area_id"];?>" <?php for($j=0;$j<count($arr_qualified);$j++){if($arr_qualified[$j]==$obj_qualified["data"][$i]["area_name"]){echo "selected='selected'";}}?>><?php echo $obj_qualified["data"][$i]["area_name"];?></option>
							<?php } ?>
						</select>
          </dd>
        </dl>
-->
<!--
        <dl>
          <dt>年間予算</dt>
          <dd>
            <select name="allyear" id="allyear">
              <option value=""></option>
						<?php for ($i = 0; $i < $count_allyear; $i++){ ?>
							<option value="<?php echo $obj_allyear["data"][$i]["area_id"];?>" <?php if($allyear==$obj_allyear["data"][$i]["area_name"]){echo "selected='selected'";}?>><?php echo $obj_allyear["data"][$i]["area_name"];?></option>
						<?php } ?>
            </select>
          </dd>
        </dl>
-->
        <dl>
          <dt>メール通知</dt>
          <dd>
            <select name="mail_flg" id="mail_flg">
              <option value="1" <?php if($mail_flg=="1"){echo "selected='selected'";}?>>全ての新着</option>
              <option value="2" <?php if($mail_flg=="2"){echo "selected='selected'";}?>>緊急の投稿のみ</option>
              <option value="3" <?php if($mail_flg=="3"){echo "selected='selected'";}?>>受け取らない</option>
            </select>
          </dd>
        </dl>
<!--
        <dl>
          <dt>自己紹介</dt>
          <dd><textarea cols="50" maxlength="300" name="introduction" rows="10"><?php echo $introduction;?></textarea>
          </dd>
        </dl>
-->
        <div id="submit">
          <input name="commit" type="button" value="保存" onclick='moveConfirm();'/>
        </div>
				<input type="hidden" name='h_software' value="<?php echo $software;?>"/>
				<input type="hidden" name='h_hardware' value="<?php echo $software;?>"/>
				<input type="hidden" name='h_qualified' value="<?php echo $software;?>"/>
      </form>
    </div>
  </section>
</div>
<!--/#wrapper-->
<?php include "footer.php"; ?>
<script src="js/chosen.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-deselect'  : {allow_single_deselect:true},
		'.chosen-select-no-single' : {disable_search_threshold:10},
		'.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		'.chosen-select-width'     : {width:"95%"}
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}
</script>
<script>
$(function() {
    $('#button').click(function(){
    $(this).next('#questionArea').slideToggle();
	$("#button").toggleClass("active");
    });
});
</script>

<script>
	function moveConfirm() {
		//var val_software = $("#software").multiselect("getChecked").map(function(){return this.value;}).get();
		//var val_hardware = $("#hardware").multiselect("getChecked").map(function(){return this.value;}).get();
		//var val_qualified = $("#qualified").multiselect("getChecked").map(function(){return this.value;}).get();

		//document.upform.h_software.value=val_software;
		//document.upform.h_hardware.value=val_hardware;
		//document.upform.h_qualified.value=val_qualified;

		//submit
		document.upform.action="?action=save";
		document.upform.submit();
	}
</script>
</body>
</html>